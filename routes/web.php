<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->options('{all:.*}', function () use ($router) {return response()->json(array(''), 200);});
$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post('/user/login', 'AuthController@authenticate');
$router->get('/logout/{id}', 'UserController@logoutUser');

$router->post('/forgotpass', 'UserController@ForgotPass');
$router->post('/change-password' ,'UserController@changePassword');


$router->get('/cancel-transaction/{id}', 'StripeController@cancelTransaction');
$router->post('/login-plat', 'DefaultController@loginwithFbGoogle');
$router->post('/signup', 'DefaultController@signup');
$router->get('/all-dogs-listing', 'DogsController@getDogs');
$router->get('/single-dog/{id}', 'DogsController@getSingleDog');
$router->get('/filter-data', 'filterController@getFilterdata');
$router->get('/get-cities/{id}', 'FilterController@getCitydata');

$router->post('/validate-phone', 'DefaultController@checkPhone');
$router->get('/get-dogbreeds', 'filterController@getDogsbreeddata');
$router->get('/getstates', 'FilterController@getstatesdata');
$router->get('/get-dogcolors', 'filterController@getdogcolordata');

$router->post('get-Filterdlist-Dogs', 'filterController@getFilterdlistDogs');
  $router->get('/breedersdog/{id}', 'DogsController@getBreedersDogs');
  
  $router->get('/get-plans', 'BreederplansController@getPlans');
$router->get('/get-specificuserdetails/{id}', 'UserController@getUserforTrans');
$router->get('/push-message/{to}/{from}', 'PushNotificationController@PushNoti');

$router->post('/pay-fordog', 'StripeController@BuyDogPay');
$router->get('/get-virtualcartdetails/{id}', 'VirtualCartController@getDetailsVirtualCart');

$router->post('/action', 'UserController@CounterAction');

$router->get('/get-speciality', 'SpecialityController@getSpeciality');
//---------------------leads api--------------------------------------------   

$router->post('/subscribe-email', 'LeadsController@newsletteremail');

$router->post('/save-lead', 'LeadsController@saveleads');
$router->get('/getuser/{id}', 'UserController@getUserdata');
//---------------------agency api--------------------------------------------   
$router->get('/get-agencies/{id}', 'AgencyController@getAgencies');
$router->get('/get-city-agencies/{slug}/{city}/{page}', 'AgencyController@getAgenciesbyCity');
$router->get('/get-state-agencies/{slug}/{state}/{page}', 'AgencyController@getAgenciesbyState');
$router->get('/get-agency/{id}', 'AgencyController@getSingleAgency');


$router->get('/checkapi', 'AdminController@checkapi');
$router->get('/get-categories', 'AgencyController@getAgenciesWithCategory');


//-----------------get speciality------------------------------------
$router->get('/getallspeciality', 'SpecialityController@getSpecialityCategory');
$router->get('/getspeciality/{id}', 'SpecialityController@getSingleSpeciality');
$router->post('/getall-groupedsubspeciality', 'SpecialityController@getSubSpecialityGroupedCategory');
$router->post('/getallSubspeciality', 'SpecialityController@getSubSpecialityCategory');

$router->get('/getallSubspeciality/{slug}', 'SpecialityController@getSingleSubSpecialityCategory');
$router->get('/getSubspecialityid', 'SpecialityController@getSpecialityID');


$router->get('/getSubspecialitydata/{splslug}/{slug}', 'SpecialityController@getSingleSubSpecialityData');

$router->get('/get-blogs', 'BlogController@getBlogs');
$router->get('/get-blog/{id}', 'BlogController@getSingleBlog');


$router->post('/get-searchedagencies/{id}', 'AgencyController@getParticularAgencies');
$router->post('/testhook', 'AgencyController@updateCustomer');
$router->post('/get-subscriptions-events', 'StripeController@checkEvents');
$router->get('/get-analytics/{id}', 'LeadsController@get7daysleadsCount');
$router->get('/get-plans', 'PlanController@getPlans');


$router->get('/get-docs/{id}', 'AgencyController@getDoctors');

$router->post('/check-email', 'UserController@checkEmail');
$router->post('/update-password', 'UserController@changePassword');
// $router->get('/get-agency-users/{id}', 'AdminController@getUsersManager');

$router->group(['prefix' => 'user', 'middleware' => 'user.auth'], function () use ($router) {
$router->post('/create-subadmin', 'AdminController@AddSubAdmin');

$router->post('/update-subadmin', 'AdminController@UpdateSubAdmin');
// $router->delete('/delete-subadmin/{id}', 'AdminController@DeleteSubAdmin');

$router->get('/get-subadmins', 'SubAdminController@getSubAdmins');
$router->delete('/delete-subadmin/{id}', 'SubAdminController@deletesubadmin');

$router->get('/get-plan/{id}', 'PlanController@getSinglePlans');

// $router->post('/update-subadmin', 'SubAdminController@UpdateSubadmin');
$router->post('/changesubadminstatus', 'SubAdminController@enabledisableStatus');
$router->get('/get-analytics/{id}', 'LeadsController@get7daysleadsCount');
$router->post('/update-seo','UserController@updateSeoTags');
$router->post('/updatepassword','UserController@updatePassword');
// -------------leads apis------------------------------------
$router->post('/addcomment', 'LeadsController@addComment');
$router->get('/getLead/{id}', 'LeadsController@getSingleLead');

$router->post('/update-peronalinfo', 'UserController@updateUserPersonalInfo');
$router->post('/update-video', 'UserController@updateUserVideo');
$router->get('/introjs/{id}', 'UserController@introStatus');
$router->post('/update-agency', 'UserController@updateUser');
$router->post('/update-agencyLogo', 'UserController@updatehospitallogo');
$router->post('/update-agencybanner', 'UserController@updatehospitalbanner');

$router->get('/get-agency-users/{id}', 'AdminController@getUsersManager');
$router->get('/getaproovedusers', 'AdminController@aproovedUsers');
$router->get('/getunaproovedusers', 'AdminController@NotaproovedUsers');
$router->post('/add-speciality', 'AdminController@addSpeciality');

$router->post('/personalinfo', 'DefaultController@regOne');
$router->post('/businessinfo', 'DefaultController@regTwo');

$router->post('/business-info', 'DefaultController@proffessionalData');
$router->post('/choose-plan', 'DefaultController@regChosePlan');
$router->post('/speciality-info', 'DefaultController@specialityData');

$router->post('/update-blog', 'BlogController@UpdateBlog');
$router->post('/add-blog', 'BlogController@addBlog');
$router->delete('/delete-blog/{id}', 'BlogController@deleteBlogs');

//---------------------ADmin api--------------------------------------------   
$router->post('/aprroveuser/{id}', 'AdminController@aprooveStatus');

//---------------------stripe api--------------------------------------------   
$router->post('/makepayment', 'StripeController@createstripecustomer');

$router->post('/addnewcard', 'StripeController@AddNewCard');
$router->get('/get-card-details/{id}', 'StripeController@GetCardDetails');
//---------------------leads api--------------------------------------------   
$router->get('/get-lead/{id}/{pagenum}/{status}/{date}/{name}', 'LeadsController@getLeads');
--
$router->post('/update-lead-status', 'LeadsController@upDateStatus');
//---------------------agency api--------------------------------------------   
$router->post('/add-doctor', 'AgencyController@addDoctor');

$router->post('/update-doctor', 'AgencyController@updateDoctor');

$router->delete('/delete-doctor/{id}', 'AgencyController@deleteDoctor');
//------------speciality-------------------------------
$router->post('/add-testimonial', 'SpecialityController@addTestimonial');
$router->post('/update-speciality-data', 'SpecialityController@UpdateDescSpeciality');
$router->post('/add-statespl-data', 'SpecialityController@addStateSplDescp');
$router->post('/add-cityspl-data', 'SpecialityController@addCitySplDescp');
//---------------------transactions api--------------------------------------------   
$router->get('/get-Transactions', 'TransactionsController@getTransactions');
$router->get('/get-Transaction/{id}', 'TransactionsController@getSingleTransaction');
});



















