<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class UserController extends BaseController {
    
   public function getUserdata(Request $request ,$id) {
     $user_id = $request->input('user_id');
        $user = DB::table('users')->select('user_id','user_type', 'subadmin_status', 'user_status', 'agency_parent_id', 'doc_speciality', 'doc_subcategory', 'doc_treatments', 'doc_treatmentsoverview', 'name', 'email', 'phone', 'address', 'state', 'city', 'hospital_name', 'hospital_address', 'hospital_state', 'hospital_city', 'hospital_phone', 'hospital_email', 'hospital_website', 'hospital_overview', 'hospital_logo', 'hospital_banner', 'hospital_video_type', 'hospital_video_key', 'hospital_video', 'hospital_latitude', 'hospital_longitude', 'is_personal_info', 'is_professional_info', 'is_speciality_info', 'is_status', 'is_payment', 'is_plan', 'is_completed', 'call_action', 'visitor_action', 'is_introjs', 'reg_date', 'reg_time', 'plan_id', 'plan_limit', 'is_practice_introjs', 'meta_title', 'meta_desc', DB::raw('CONCAT("https://www.api.medicalmarketingsolutionprojects.com/public/profilpictures/", "",profilepic) AS profilepic'))->where('user_id', $id)->first();
        
            if($user->doc_speciality){
                $expspls = explode(",",$user->doc_speciality);
                 $spls = DB::table('speciality')->whereIn('speciality_id', $expspls)->where('speciality_subcategory_id',0)->pluck('speciality_name');
                 $user->spl_names = $spls;
           
            }
            if($user->doc_subcategory){
                 $expsubspls = explode(",",$user->doc_subcategory);
                 $subspls = DB::table('speciality')->whereIn('speciality_subcategory_id', $expsubspls)->pluck('speciality_subcategory_id_name');
                 $user->sub_spl_names = $subspls;
            }
            if($user->plan_id){
                 $plan_id = $user->plan_id;
                  $plan =   DB::table('user_plans')->select('plan_amount','plan_cycle','product_name','plan_points')->where([['status','active'],['uniquecode',$plan_id]])->first();
                 $user->plandetails = $plan;
            }
            $splids = $user->doc_speciality;
            
            $array = explode(',', $splids);
            $cateee = DB::table('speciality')->select('speciality_id','speciality_name','speciality_subcategory_id','speciality_subcategory_id_name','slug')->whereIn('speciality_id',$array)->where('speciality_subcategory_id','!=','0')->get();
            
            $formatsub = array();
            $cateee = json_decode(json_encode($cateee,true));
            foreach($cateee as $key => $cat){
                $splsslugg = DB::table('speciality')->select('slug')->where('speciality_id', $cat->speciality_id)->where('speciality_subcategory_id',0)->first();
                $formatsub[$cat->speciality_name][]= array('id' => $cat->speciality_subcategory_id,'name'=>$cat->speciality_subcategory_id_name,'spl_id'=> $cat->speciality_id,'spl_name'=>$cat->speciality_name,'subspl_slug'=>$cat->slug,'spl_slug'=>$splsslugg->slug);
            }
            
            if(count($formatsub) > 0){
                $allsubs = $formatsub;
            } else{
                 $allsubs = null;
            }
            // print_r($formatsub); die();
            
             $doctors = DB::table('doctors')->select('*')->where('agency_id',$id)->first();
             $agencyusers = DB::table('users')->select('*')->where('agency_parent_id',$id)->first();
             
             $callactions = DB::table('user_actions')->select('*')->where([['agency_id',$id],['action_type','call_action']])->get();
             $visitoractions = DB::table('user_actions')->select('*')->where([['agency_id',$id],['action_type','visitor_action']])->get();
             
             
             if($callactions){
                  $user->callactions = count($callactions);
             }else{
                  $user->callactions = '0';
             }
             
             if($visitoractions){
                  $user->visitoractions = count($visitoractions);
             }else{
                  $user->visitoractions = '0';
             }
             
             
            if($agencyusers){
                $user->is_users = '1';
            }else{
                $user->is_users = '0';
            }
            
            $plusperc = '0';
            //  if($user->doc_speciality){
            //      $plusperc = $plusperc+5;
            //  }
            // if($user->doc_subcategory){
            //      $plusperc = $plusperc+5;
            //  }
            if($doctors){
                $user->is_doctor = '1';
                  $plusperc = $plusperc+20;
            }else{
                $user->is_doctor = '0';
            }
            if($user->doc_speciality){
                 $plusperc = $plusperc+20;
             }
             if($user->hospital_overview){
                 $plusperc = $plusperc+20;
             }
            //  if($user->name){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->email){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->phone){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->address){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->state){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->city){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->hospital_name){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->hospital_address){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->hospital_state){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->hospital_city){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->hospital_phone){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->hospital_email){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->hospital_website){
            //      $plusperc = $plusperc+5;
            //  }
            //  if($user->hospital_overview){
            //      $plusperc = $plusperc+5;
            //  }
             if($user->hospital_logo){
                 $plusperc = $plusperc+20;
             }
             if($user->hospital_banner){
                 $plusperc = $plusperc+20;
             }
              $user->profile_percentage = $plusperc;
              
               $user->allsub_specialities = $allsubs;
              
              
            if($user){
            return response()->json([
                        'ResponseCode' => '1',
                        'data' => $user,
            ],200);
        }else{
            return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'no data.',
                    ],400);
        }
    }
    
   public function updateUserPersonalInfo(Request $request) {
          $user_id = $request->input('currid');
          $name = $request->input('name');
          $phone = $request->input('phone');
          $address =  $request->input('address');
          $state = $request->input('state');
          $city =  $request->input('city');
      
      
      $userone =   DB::table('users')->where('user_id', $user_id)->first();
      if($name == $userone->name && $phone == $userone->phone && $address == $userone->address && $state == $userone->state && $city == $userone->city){
           return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'you havent updated anyhing!!.'
                    ],200);
      }
      
      
            $up =   DB::table('users')
            ->where('user_id', $user_id)
            ->update([  
                            'name' => $name,
                            'phone' => $phone,
                            'address' => $address,
                            'state' => $state,
                            'city' => $city,
                       
                       ]);
         if($up){
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'User Info updated Successfully.',
                    ],200);
         }else{
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
         }
    }
     public function updateUser(Request $request) {
          $user_id = $request->input('currid');
          $hospital_name = $request->input('hospital_name');
          $hospital_phone = $request->input('hospital_phone');
          $hospital_email =  $request->input('hospital_email');
          $hospital_website = $request->input('hospital_website');
          $hospital_overview =  $request->input('hospital_overview');
      
      
      $userone =   DB::table('users')->where('user_id', $user_id)->first();
      if($hospital_name == $userone->hospital_name && $hospital_phone == $userone->hospital_phone && $hospital_email == $userone->hospital_email && $hospital_website == $userone->hospital_website && $hospital_overview == $userone->hospital_overview){
           return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'you havent updated anyhing!!.'
                    ],200);
      }
      
      
            $up =   DB::table('users')
            ->where('user_id', $user_id)
            ->update([  
                            'hospital_name' => $hospital_name,
                            'hospital_phone' => $hospital_phone,
                            'hospital_email' => $hospital_email,
                            'hospital_website' => $hospital_website,
                            'hospital_overview' => $hospital_overview,
                       
                       ]);
         if($up){
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'User Info updated Successfully.',
                    ],200);
         }else{
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
         }
    }
    
      public function updatehospitallogo(Request $request) {
            $user_id = $request->input('currid');
            $hospital_logo = md5(rand(11111, 999990) . date('Ymdhis')) . "." . $request->file('hospital_logo')->getClientOriginalExtension();
            $path = getcwd();
            $validator = Validator::make($request->all(), [
                'hospital_logo' => 'required|file',
            ]);
            if ($validator->fails()) {
                 return response()->json([
                    'ResponseCode' => '0',
                    'ResponseText' => $validator->errors()->first(),
                ]);
                
            } else {
                $dir = $path.'/hospitalimages';
                $data = DB::table('users')->where('user_id', $user_id)->update([  
                            'hospital_logo' => $hospital_logo,
                       ]);
            }
            if ($data) {

                if (!is_dir($dir)) {
                    mkdir($dir);
                }

                $request->file('hospital_logo')->move("$dir", $hospital_logo);

                return response()->json([
                    'ResponseCode' => '1',
                    'ResponseText' => 'Practice Logo Updated Successfully',
                ]);
            } else {
                return response()->json([
                    'ResponseCode' => '0',
                    'ResponseText' => 'Error Occured Please Try Again',
                ]);
            }
        
        
    }
    
    public function updatehospitalbanner(Request $request) {
            $user_id = $request->input('currid');
            $hospital_banner = md5(rand(11111, 999990) . date('Ymdhis')) . "." . $request->file('hospital_banner')->getClientOriginalExtension();
            $path = getcwd();
            $validator = Validator::make($request->all(), [
                'hospital_banner' => 'required|file',
            ]);
            if ($validator->fails()) {
                 return response()->json([
                    'ResponseCode' => '0',
                    'ResponseText' => $validator->errors()->first(),
                ]);
                
            } else {
                $dir = $path.'/hospitalimages';
                $data = DB::table('users')->where('user_id', $user_id)->update([  
                            'hospital_banner' => $hospital_banner,
                       ]);
            }
            if ($data) {

                if (!is_dir($dir)) {
                    mkdir($dir);
                }

                $request->file('hospital_banner')->move("$dir", $hospital_banner);

                return response()->json([
                    'ResponseCode' => '1',
                    'ResponseText' => 'Practice banner Updated Successfully',
                ]);
            } else {
                return response()->json([
                    'ResponseCode' => '0',
                    'ResponseText' => 'Error Occured Please Try Again',
                ]);
            }
        
        
    }
    
       public function CounterAction(Request $request) {
        $actiontype = $request->input('actiontype');
        $agency_id = $request->input('agency_id');
        $userdata = DB::table('users')->select('call_action','visitor_action')->where('user_id', $agency_id)->first();
        $call_action = $userdata->call_action;
        $visitor_action = $userdata->visitor_action;
        $dd = date("d");
          $mm = date("m");
          $yyyy = date("Y");
                if($actiontype == 'callaction'){
                    $action_type = 'call_action';
                    $add1callaction = $call_action+1;
                    
                     $up =   DB::table('users')
                    ->where('user_id', $agency_id)
                    ->update([  
                                    'call_action' => $add1callaction,
                              ]);
                    $up =   DB::table('user_actions')
                    ->insert([  
                                    'action_type' => $action_type,
                                    'agency_id' => $agency_id,
                                    'dd'=> $dd,
                                    'mm' => $mm,
                                    'yyyy' => $yyyy
                               ]);
                    
                }else if($actiontype == 'visitoraction'){
                    $add1visitoraction = $visitor_action+1;
                    
                      $up =   DB::table('users')
                    ->where('user_id', $agency_id)
                    ->update([  
                                    'visitor_action' => $add1visitoraction,
                              ]);
                    $action_type = 'visitor_action';
                     $up =   DB::table('user_actions')
                    ->insert([  
                                    'action_type' => $action_type,
                                    'agency_id' => $agency_id,
                                    'dd'=> $dd,
                                    'mm' => $mm,
                                    'yyyy' => $yyyy
                               ]);       
                }
        
            if($up){
            return response()->json([
                        'ResponseCode' => '1',
                         'ResponseText' => 'done.',
            ],200);
        }else{
            return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'error please try again.',
                    ],400);
        }
        
    }
    
     public function introStatus(Request $request,$id){
                 $Introstat =  DB::table('users')->where('user_id',$id)->update(
                                [
                                    'is_introjs' => '1',
                                ]);
              if($Introstat){
                     return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }           
                                
            }
    public function ForgotPass(Request $request) {
            $email = $request->input('email');
        $user = DB::table('users')->select('*')->where('email', $email)->first();
        if($user){
            $userid = $user->user_id;
            $firstname = $user->name;
            $phone = $user->phone;
            // $user = DB::table('change_password')->where('user_id', $userid)->delete();
            $code = rand(100000,1000000);
                            // $inser  =   DB::table('change_password')->insert(
                            //     [
                            //         'user_id' => $userid,
                            //         'email' => $email,
                            //         'code' => $code,
                            //     ]
                            // );
                            try {
                            $account_sid = getenv("TWILIO_SID");
                            $auth_token = getenv("TWILIO_AUTH_TOKEN");
                            $twilio_number = getenv("TWILIO_NUMBER");
                            $message = "You have requested for a change of password, this is your code ".$code;
                            $correctmessage = str_replace("%20"," ",$message);
                            $client = new Client($account_sid, $auth_token);
                            $gg = $client->messages->create($phone, 
                            ['from' => $twilio_number, 'body' => $correctmessage] );
                           
                            } catch (\Exception $e){
                						if($e->getCode() == 21211)
                						{}
                			}
                            // $useremail = $request->input('email');
                            // // $firstname = $request->input('firstname');
                            // $email = new \SendGrid\Mail\Mail();
                            // $link = 'https://www.medicalmarketingsolutionprojects.com/';
                            // $from = 'no-reply@www.medicalmarketingsolutionprojects.com';
                            // $message = "Hello ". $firstname."!". "<br>" . "<br>" . "You have requested for a change of password, this is your code .". "<br>" .
                            //  $code;
                            // $email->setFrom("$from", "USA Health MD");
                            // $email->setSubject("Password Reset");
                            // $email->addTo($useremail, $firstname);
                            // $email->addContent("text/plain", $message);
                            // $email->addContent("text/html", $message);
                            // $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
                            // $sendgrid->send($email);
                            
                            
            return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Email sent .',
                                'code' => $code
                    ]);
        }else{
            return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'This email Doesent exist .',
                    ]);
        }
    }
 
     public function changePassword(Request $request) {
          $newpassword = $request->input('password');
          $email = $request->input('email');
           
           $changed =  DB::table('users')
                ->where('email', $email)
                ->update([  
                    'password' => Hash::make($newpassword),
               ]);
               if($changed){
                    return response()->json([
                            'ResponseCode' => '1',
                            'ResponseText' => 'Your Password is updated.',
                ]);
               }else{
               }
    }
        public function checkEmail(Request $request) {
            $email = $request->input('email');
        $user = DB::table('users')->select('*')->where('email', $email)->first();
        if($user){
            $userid = $user->user_id;
            $firstname = $user->hospital_name;
            $phone = $user->phone;
            // $user = DB::table('change_password')->where('user_id', $userid)->delete();
             $code = rand(100000,1000000);
            //  $inser  =   DB::table('change_password')->insert(
            //                     [
            //                         'user_id' => $userid,
            //                         'email' => $email,
            //                         'code' => $code,
            //                     ]
            //                 );
                            if($phone){
                              try {
                                $account_sid = getenv("TWILIO_SID");
                                $auth_token = getenv("TWILIO_AUTH_TOKEN");
                                $twilio_number = getenv("TWILIO_NUMBER");
                                // $phone = $request->input('phone');
                               $message = "You have requested for a change of password, this is your OTP ".$code;
                                $correctmessage = str_replace("%20"," ",$message);
                                $client = new Client($account_sid, $auth_token);
                                $gg = $client->messages->create($phone, 
                                ['from' => $twilio_number, 'body' => $correctmessage] );
                               
                                } catch (\Exception $e){
                    						if($e->getCode() == 21211)
                    						{}
                    			}  
                            }
                            
                            
                            $useremail = $request->input('email');
                            // $firstname = $request->input('firstname');
                            $email = new \SendGrid\Mail\Mail();
                             $from = 'info@usahealthmd.com';
                              $clientname = 'USA Health MD';
                            $message = '<div class="" style="float:none;width:650px;margin:auto;">
                                    	<div style="float: left;width: 100%;background: #f8f8f8; padding: 0 0px;">
                                    		<p style="width:100%;float:left;height: 140px;background: #1eb7e4;margin: 0 0;clip-path: polygon(50% 0%, 100% 0, 100% 50%, 50% 26%, 50% 20%, 50% 23%, 60% 25%, 54% 25%, 0% 55%, 0 0);"></p>
                                    	     <table style=" width: 100%;float: left;text-align: center;">
                                    			<tbody>
                                    				<tr style=" width: 100%;float: left;"><td style=" width: 100%;float: left;"><img style="width: 200px;margin-top: -62px;" src="https://www.usahealthmd.com/assets/images/logo.png"></td>
                                    				</tr>
                                    			   </tbody>
                                    		      </table>
                                    		<table style="float: left;width: 100%;margin-top: 2px;padding: 0 12px;">
                                    		<thead style="float: left;width: 100%;">
                                    	         <tr style="float: left;width: 100%;">
                                    				<td style="float: left;width: 100%;text-align: center;"><h2 style="color: #1eb7e4;font-size: 26px;">Hello '.$firstname.'</h2></td>
                                    				<td style="float: left;width: 100%;"><p style="float: left;width: 100%;margin:0px;text-align: center;color: #575454;font-size: 22px;line-height: 30px;">You have requested for a change of password, this is your code <b>'.$code.'</b> </p></td>
                                    			</tr>
                                    	   </tr>
                                    	  </thead>
                                    	 </table>
                                    	</div>
                                    	</div>' .
                            //  $code;
                            $email->setFrom("$from", $clientname);
                            $email->setSubject("Password Reset");
                            $email->addTo($useremail, $firstname);
                            $email->addContent("text/plain", $message);
                            $email->addContent("text/html", $message);
                            $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
                            $sendgrid->send($email);
                            
                            
            return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Email sent .',
                                'code' => $code
                    ]);
        }else{
            return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'This email Does not exist .',
                    ]);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
          public function getUsers(Request $request) {
                $user = DB::table('users')->select('user_id','user_type', 'subadmin_status', 'user_status', 'agency_parent_id', 'doc_speciality', 'doc_subcategory', 'doc_treatments', 'doc_treatmentsoverview', 'name', 'email', 'phone', 'address', 'state', 'city', 'hospital_name', 'hospital_address', 'hospital_state', 'hospital_city', 'hospital_phone', 'hospital_email', 'hospital_website', 'hospital_overview', 'hospital_logo', 'hospital_banner', 'hospital_video_type', 'hospital_video_key', 'hospital_video', 'hospital_latitude', 'hospital_longitude', 'is_personal_info', 'is_professional_info', 'is_speciality_info', 'is_status', 'is_payment', 'is_plan', 'is_completed', 'call_action', 'visitor_action', 'is_introjs', 'reg_date', 'reg_time', 'plan_id', 'plan_limit', 'is_practice_introjs', 'meta_title', 'meta_desc')->where('user_type','user')->get();
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }
           
        
    }
              public function checkAddress(Request $request,$id) {
                $user = DB::table('users')->select('delivery_address')->where('user_id',$id)->first();
                if($user){
                    if($user->delivery_address){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => json_decode($user->delivery_address),
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '1',
                            'ResponseText' => 'no address',
                        ]);
                }
                }else{
                      return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }
                
           
        
    }
     public function getNotverifiedUsers(Request $request) {
                $user = DB::table('users')->select('user_id','user_type', 'subadmin_status', 'user_status', 'agency_parent_id', 'doc_speciality', 'doc_subcategory', 'doc_treatments', 'doc_treatmentsoverview', 'name', 'email', 'phone', 'address', 'state', 'city', 'hospital_name', 'hospital_address', 'hospital_state', 'hospital_city', 'hospital_phone', 'hospital_email', 'hospital_website', 'hospital_overview', 'hospital_logo', 'hospital_banner', 'hospital_video_type', 'hospital_video_key', 'hospital_video', 'hospital_latitude', 'hospital_longitude', 'is_personal_info', 'is_professional_info', 'is_speciality_info', 'is_status', 'is_payment', 'is_plan', 'is_completed', 'call_action', 'visitor_action', 'is_introjs', 'reg_date', 'reg_time', 'plan_id', 'plan_limit', 'is_practice_introjs', 'meta_title', 'meta_desc')->where([['user_type','breeder'],['is_status','0']])->get();
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }
           
        
    }
    
    
            public function insFirebase(Request $request){
                 $firebaseid = $request->input('firebaseid');
                  $userid = $request->input('userid');
                  
                  $curruser = DB::table('users')->select('firebaseid')->where('user_id',$userid)->first();
                  $oldfire = $curruser->firebaseid;
                  if($oldfire == $firebaseid){
                      return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'already exists',
                    ]);
                  }
                  
                  
                 $insertfirebaseid =  DB::table('users')->where('user_id',$userid)->update(
                                [
                                    'firebaseid' => $firebaseid,
                                ]);
              if($insertfirebaseid){
                     return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }           
                                
            }
           
          public function disableNoti(Request $request,$id) {
              $notification = $request->input('notification');
              $user = DB::table('users')->select('disable_noti')->where('user_id',$id)->first();
              $disable_noti = $user->disable_noti;
              
              if($notification == 'enable'){
                  $upval = '0';
                  $mess = 'Notifications Enabled';
              }else{
                   $upval = '1';
                   $mess = 'Notifications Disabed';
              }
              
            //   if($disable_noti == '1'){
            //       $upval = '0';
            //       $mess = 'Notifications Enabled';
            //   }else{
            //       $upval = '1';
            //       $mess = 'Notifications Disabed';
            //   }
                 $up =   DB::table('users')
            ->where('user_id', $id)
            ->update([  
                            'disable_noti' => $upval,
                       ]);
                if($notification){
                     return response()->json([
                          'ResponseCode' => '1',
                                'ResponseText' => $mess,
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }
           
        
    }
    
              public function deleteAccount(Request $request,$id) {
                    $userdata = DB::table('users')->select('*')->where('user_id', $id)->first();
                    $month = date('m');
                    $year = date("Y");
                    $randnum = rand(1111111111,9999999999);
                    date_default_timezone_set("America/Mexico_City");
                    $currtime = date("H:i:s");
                    $currdate = date("Y-m-d");
                    $insertdeletedtable =  DB::table('deleted-users')->insert(
                        [
                            'name' => $userdata->name,
                            'old_user_id' => $userdata->user_id,
                            'user_id' => $randnum,
                            'status'=> 'deleted',
                            'phone'=> $userdata->phone,
                            'state'=> $userdata->state,
                            'profile_pic'=> $userdata->profile_pic,
                            'street'=> $userdata->street,
                            'address'=> $userdata->address,
                            'city'=> $userdata->city,
                            'zip'=> $userdata->zip,
                            'country'=> $userdata->country,
                            'tnc'=> $userdata->tnc,
                            'disable_noti'=> $userdata->disable_noti,
                            'reg_date'=>$userdata->reg_date,
                            'user_type' => $userdata->user_type,
                            'email' =>  $userdata->email,
                            'password' => $userdata->password,
                            'reg_time' => $userdata->reg_time,
                            'login_method' => $userdata->login_method,
                            'platform_id' => $userdata->platform_id,
                            'del_date'=> $currdate,
                            'del_time'=>$currtime,
                        ]);
                         $upp = DB::table('users')->where('user_id', $id)->update([
                             'status'=> 'deleted',
                             ]);
                if($insertdeletedtable){
                    //  $upp = DB::table('users')->where('user_id', $id)->delete();
                     return response()->json([
                          'ResponseCode' => '1',
                                'ResponseText' => 'your account is deleted. if you want to recover this account contact with support',
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }
           
        
    }
    
     public function updatePassword(Request $request) {
          $user_id = $request->input('currid');
          $oldpassword = $request->input('oldpassword');
          $newpassword = $request->input('newpassword');
          $confirmpassword = $request->input('confirmpassword');
      
      $validator = Validator::make($request->all(), [
                'currid' => 'required',
                'oldpassword' => 'required',
                'newpassword' => 'required',
                'confirmpassword' =>'required'
                ]);
      
      if ($validator->fails()) {
                    $response = $validator->errors()->first();
                    // return response(json_encode($response));
                    return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => $response
                        ]);
                } else {
                    
                       $userone =   DB::table('users')->where('user_id', $user_id)->first();
                       if($userone){
                           if(Hash::check($oldpassword, $userone->password)){
                              if($newpassword == $confirmpassword){
                                  $up =   DB::table('users')
                                ->where('user_id', $user_id)
                                ->update([  
                                                'password' =>  Hash::make($confirmpassword),
                                           
                                           ]);
                              }else{
                                     return response()->json([
                                                    'ResponseCode' => '0',
                                                    'ResponseText' => 'passwords doesnt match!!.'
                                        ],200); 
                              }
                              
                          }else{
                               return response()->json([
                                                    'ResponseCode' => '0',
                                                    'ResponseText' => 'The old password you entered is incorrect!!.'
                                        ],200);
                          }
                       }else{
                              return response()->json([
                                                    'ResponseCode' => '0',
                                                    'ResponseText' => 'this is not a user!!.'
                                        ],200);
                       }
                          
                    
                }
     
      
      
            
     
         if($up){
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Password updated Successfully.',
                    ],200);
         }else{
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
         }
                    
        
    }
    
    public function getUserWishlist(Request $request,$id) {
        $user = DB::table('users')->select('wishlist')->where('user_id',$id)->first();
        $wishlistdata = json_decode($user->wishlist);
        
            if($user){
                return response()->json([
                     'ResponseCode' => '1',
                        'wishlist' => $wishlistdata,
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '0',
                    'ResponseText' => 'error try again',
                ]);
            }
            
        
    }
    public function getUserforTrans(Request $request,$id) {
        $user = DB::table('users')->select('user_id','email','name')->where('user_id',$id)->first();

            if($user){
                return response()->json([
                     'ResponseCode' => '1',
                        'data' => $user,
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '0',
                    'ResponseText' => 'error try again',
                ]);
            }
            
        
    }
            

  public function deleteUser(Request $request) {
     
          $user_id = $request->input('user_id');
         $del = DB::table('users')->where('user_id', $user_id)->delete();
         if($del){
               return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'user Deleted Successfully.',
                    ]);
         }else{
              return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'error please try again.',
                    ]);
         }
                  
        
    }
      public function updatePhone(Request $request) {
          $user_id = $request->input('currid');
          $phone =  $request->input('phone');
        // $password = $request->input('password');
      
            $up =   DB::table('users')
            ->where('user_id', $user_id)
            ->update([  
                            'phone' => $phone,
                            'tnc' => '1'
                       ]);
     
         if($up){
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Phone updated Successfully.',
                    ],200);
         }else{
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
         }
                    
        
    }
      public function addDogtowishlist(Request $request) {
          $user_id = $request->input('curr_id');
          $dog_id =  $request->input('dog_id');
          $userdata = DB::table('users')->where('user_id', $user_id)->first();
          $wishlist = $userdata->wishlist;
          $dogarray = array();
          if($wishlist){
              $dogs_array = json_decode($wishlist);
          }else{
               $dogs_array = $dogarray;
          }
 
         if (in_array($dog_id, $dogs_array))
              {
              $darray = array();
                for ($x = 0; $x < count($dogs_array); $x++) {
                  if($dogs_array[$x] == $dog_id){
                    //   unset($dogs_array[$x]);
                  }else{
                      array_push($darray,$dogs_array[$x]);
                  }
                }
                $dogs_array = $darray;
                $message = 'Dog Removed from your wishlist.'; 
              }
            else
              {
                 array_push($dogs_array,$dog_id);
                   $message = 'Dog Added to your wishlist.'; 
              }

            $up =   DB::table('users')
            ->where('user_id', $user_id)
            ->update([  
                            'wishlist' => json_encode($dogs_array),
                      ]);
     
         if($up){
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => $message,
                    ],200);
         }else{
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
         }
                    
        
    }
    
     public function addDogtoCart(Request $request) {
          $user_id = $request->input('curr_id');
          $dog_id =  $request->input('dog_id');
          $addorremove =  $request->input('addorremove');
          
        
          $userdata = DB::table('users')->where('user_id', $user_id)->first();
          $cart = $userdata->cart;
          if($userdata->wishlist){
              $wishlist = json_decode($userdata->wishlist);
          }else{
              $wishlist = array();
          }
          
          $dogarray = array();
          
 
         if($addorremove === 'add'){
             
             if($cart){
            //   $dogs_array = json_decode($cart);
                return response()->json([
                                    'ResponseCode' => '0',
                                    'ResponseText' => 'you cannot add more then one dog in your cart',
                            ],200);
              }else{
                   $dogs_array = $dogarray;
              }
             
                if (in_array($dog_id, $dogs_array))
                    {
                        $message = 'Dog is already in your cart.'; 
                        return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => $message,
                        ],200);
                    }
                else
                    {
                     array_push($dogs_array,$dog_id);
                       $message = 'Dog Added to your cart.'; 
                    }
                    
                     //--------------if add in cart then remove from wishlist-------------------
              
                          if (in_array($dog_id, $wishlist))
                          {
                           $d = array();
                            for ($x = 0; $x < count($wishlist); $x++) {
                              if($wishlist[$x] == $dog_id){
                               
                              }else{
                                  array_push($d,$wishlist[$x]);
                              }
                            }
                            $wishlist_array = $d;
                          }
                        else
                          { $wishlist_array = $wishlist;  }
                    //--------------if add in cart then remove from wishlist-------------------
          }else{
                  if (in_array($dog_id, $dogs_array))
                {
                  $darray = array();
                    for ($x = 0; $x < count($dogs_array); $x++) {
                      if($dogs_array[$x] == $dog_id){
                          
                      }else{
                          array_push($darray,$dogs_array[$x]);
                      }
                    }
                    $dogs_array = $darray;
                    $message = 'Dog Removed from your cart.'; 
                }
                else
                    {
                    //  array_push($dogs_array,$dog_id);
                    //   $message = 'Dog Added to your cart.'; 
                    }
                    
                     //--------------if add in cart then remove from wishlist-------------------
              
                          if (in_array($dog_id, $wishlist))
                          {
                           $d = array();
                            for ($x = 0; $x < count($wishlist); $x++) {
                              if($wishlist[$x] == $dog_id){
                               
                              }else{
                                  array_push($d,$wishlist[$x]);
                              }
                            }
                            $wishlist_array = $d;
                          }
                        else
                          { $wishlist_array = $wishlist;  }
                    //--------------if add in cart then remove from wishlist-------------------
          }
          
            //     if (in_array($dog_id, $dogs_array))
            //     {
            //       $darray = array();
            //         for ($x = 0; $x < count($dogs_array); $x++) {
            //           if($dogs_array[$x] == $dog_id){
                          
            //           }else{
            //               array_push($darray,$dogs_array[$x]);
            //           }
            //         }
            //         $dogs_array = $darray;
            //         $message = 'Dog Removed from your cart.'; 
            //     }
            //     else
            //         {
            //          array_push($dogs_array,$dog_id);
            //           $message = 'Dog Added to your cart.'; 
            //         }
        
            //   //--------------if add in cart then remove from wishlist-------------------
              
            //               if (in_array($dog_id, $wishlist))
            //               {
            //               $d = array();
            //                 for ($x = 0; $x < count($wishlist); $x++) {
            //                   if($wishlist[$x] == $dog_id){
                               
            //                   }else{
            //                       array_push($d,$wishlist[$x]);
            //                   }
            //                 }
            //               $wishlist_array = $d;
            //               }
            //             else
            //               { $wishlist_array = $wishlist;  }
            //       //--------------if add in cart then remove from wishlist-------------------
              

            $up =   DB::table('users')
            ->where('user_id', $user_id)
            ->update([  
                            'cart' => json_encode($dogs_array),
                            'wishlist'=> json_encode($wishlist_array),
                      ]);
     
         if($up){
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => $message,
                    ],200);
         }else{
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
         }
                    
        
    }
    
    
    
             public function getWishlistDogs(Request $request,$id) {
                    $user = DB::table('users')->select('wishlist')->where('user_id', $id)->first();
                    $ids = json_decode($user->wishlist);
                      $dogs = DB::table('dogs_list')->select('*', DB::raw('CONCAT("https://api.thepuppyapp.com/public/dogs/", "",featured_image) AS featured_image'))->whereIn('dog_id', $ids)->get();
                       if($dogs){
                            return response()->json([
                                'ResponseCode' => '1',
                                'data' => $dogs,
                            ],200);
                       }else{
                            return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                            ],400);
                       }
                       
                        
                    
                }
         public function getCartDogs(Request $request,$id) {
                    $user = DB::table('users')->select('cart')->where('user_id', $id)->first();
                    if($user->cart){
                        $ids = json_decode($user->cart);
                    }else{
                        $ids = array();
                    }
                    
                      $dogs = DB::table('dogs_list')->select('*', DB::raw('CONCAT("https://api.thepuppyapp.com/public/dogs/", "",featured_image) AS featured_image'))->whereIn('dog_id', $ids)->get();
                       
                       $sum = 0;
                        foreach($dogs as $key=>$value)
                        {
                          $sum+= $value->price;
                     
                        }
                                     
                     $array1 = array('dogs'=>json_decode(json_encode($dogs), TRUE));
                     $array2 = array('Subtotal'=>$sum);
                     $output = array_merge($array1, $array2);
                      
                      if($output) {
                           return response()->json([
                             'ResponseCode' => '1',
                             'data' => $output,
                        ],200);
                      }else{
                           return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
                      }
                       
                    
                }
    
    //             public function updatePassword(Request $request) {
    //      $oldpassword = $request->input('oldpassword');
    //       $newpassword = $request->input('newpassword');
    //       $newconfirmpassword = $request->input('newconfirmpassword');
    //       $currid = $request->input('currid');
           
           
    //       $user = User::where('user_id', $currid)->first();
    //     if($newpassword == $newconfirmpassword){
    //         if (Hash::check($oldpassword, $user->password)) {
    //                  DB::table('users')
    //                     ->where('user_id', $currid)
    //                     ->update([  
    //                         'password' => Hash::make($newpassword),
    //                   ]);
    //                     return response()->json([
    //                                 'ResponseCode' => '1',
    //                                 'ResponseText' => 'Your Password is updated.',
    //                     ]);
    //         }else{
    //              return response()->json([
    //                 'ResponseCode' => '0',
    //                 'ResponseText' => 'The Old password you entered is incorrect',
    //             ]);
    //         }
    //     }else{
    //          return response()->json([
    //                 'ResponseCode' => '0',
    //                 'ResponseText' => 'password doesnt match',
    //             ]);
    //     }
    // }
   
            public function logoutUser(Request $request,$id){
                 $logout =  DB::table('users')->where('user_id',$id)->update(
                                [
                                    'fcm_token' => '0',
                                ]);
              if($logout){
                     return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }else{
                      return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }           
                                
            }
              public function updateSeoTags(Request $request) {
                   $user_id = $request->input('user_id');
                  $seo_title = $request->input('seo_title');
                  $seo_description = $request->input('seo_description');
                $up =  DB::table('users')->where('user_id',$user_id)->update(
                            [
                                'meta_title' => $seo_title,
                                'meta_desc' => $seo_description,
                                ]);
                if($up){
                     return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Meta tags Updated Successfully',
                    ]);
                }else{
                     return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Meta tags Updated Successfully',
                    ]);
                }  
                  
              }
          public function updateUserVideo(Request $request) {
              $user_id = $request->input('currid');
              $type = $request->input('type');
              $url = $request->input('url');
              $key = $request->input('key');
             $up =  DB::table('users')->where('user_id',$user_id)->update(
                                [
                                    'hospital_video_type' => $type,
                                    'hospital_video_key' => $key,
                                    'hospital_video'=> $url
                                ]);
                if($up){
                     return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Video added Successfully',
                    ]);
                }else{
                     return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Video added Successfully',
                    ]);
                }  
              
          }
}
