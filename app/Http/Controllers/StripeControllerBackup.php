<?php
namespace App\Http\Controllers;
// require_once('vendor/autoload.php');
// use App\Models\Support;
use DB;
use Illuminate\Http\Request;
use App\Models\User;
use Firebase\JWT\JWT;
use Laravel\Lumen\Routing\Controller as BaseController;
use Validator;
use Stripe\Stripe;
class StripeController extends BaseController {


 protected function jwt(User $user)
    {
        if($user->user_type == 'user'){
            $role = '#U$*43vc4*756y0&78h7$t';
        }else if($user->user_type == 'doctor'){
             $role = '$DoC#79rd*&57h%45@e';
        }else if($user->user_type == 'superadmin'){
             $role = '$$uPU67#$d#77HHJk@8';
        }else if($user->user_type == 'agency'){
             $role = '@GCPU67#$d#FR%%HJYYk@8';
        }else{
             $role = '';
        }
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->user_id, // Subject of the token
            'nik' => $user->email, // nickname of the current user
            'sat' => $user->user_status, // status of the current user
            'completed'=> (int)$user->is_completed,
            'urxrs' => $role, // user role
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 1440*365, // 1209600 //60*60 // Expiration time
            // 'exp' => 43800
        ];
        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function createstripecustomer(Request $request) {
          \Stripe\Stripe::setVerifySslCerts(false);
        /**/
        // Add new Card
        // $customerid = 'cus_Iym7X9R0pnDB63';
        // $token= $request->input('tokenn');
        // \Stripe\Stripe::setVerifySslCerts(false);
        // $stripe = new \Stripe\StripeClient(
        //   'sk_test_51IL9mZGXQwgSBKzkVsLedeaw474KpbQPOBrXHqhXlOQovxDXIlWdQ9qJK3extBaM5IIOO26LyDAEzvhHKOUSDxZI00HDf7o8Sj'
        // );
        // $stripe->customers->createSource(
        //   $customerid,
        //   ['source' => $token]
        // );
        
        // die();
        // // Delete Card
        // $customerid = 'cus_Iym7X9R0pnDB63';
        // $card_id = 'card_1IMoZbGXQwgSBKzkWbnThwxR';
        // \Stripe\Stripe::setVerifySslCerts(false);
        // $stripe = new \Stripe\StripeClient(
        //   'sk_test_51IL9mZGXQwgSBKzkVsLedeaw474KpbQPOBrXHqhXlOQovxDXIlWdQ9qJK3extBaM5IIOO26LyDAEzvhHKOUSDxZI00HDf7o8Sj'
        // );
        // $stripe->customers->deleteSource(
        //   $customerid,
        //   'card_1IMoZbGXQwgSBKzkWbnThwxR',
        //   []
        // );
        // die();
        /**/

        $token= $request->input('tokenn');
        $amount = $request->input('amount');
        $email = $request->input('email');
        $user_id = $request->input('user_id');
        $name_stripe = $request->input('name_stripe');
        $transaction_id = rand(99999 , 9999999);
        $uniquecode = md5(date('Ymdhis' . $request->input('email') . rand(11111, 99999)));
        $ip = $_SERVER['REMOTE_ADDR'];
        $month = date('m');
        $year = date("Y");
        
        
        \Stripe\Stripe::setApiKey('sk_test_51IL9mZGXQwgSBKzkVsLedeaw474KpbQPOBrXHqhXlOQovxDXIlWdQ9qJK3extBaM5IIOO26LyDAEzvhHKOUSDxZI00HDf7o8Sj');
        //   \Stripe\Stripe::setApiKey('sk_test_51Hmc0rBlXb1xQdQX31icqme6NTEqrU4DzA01BXXoY6nUD7GPk5CV3v9wevA9DOid18C0m0dLnC79qVuxZiiT3SLx00DxrUoJww');
        $email = $request->input('email');
        
        $checkstripecusentries = DB::table('stripe_customers')->select('*')->where('user_id', $user_id)->first();
         
         try {
             
             
               $customer = \Stripe\Customer::create(array(
            "email" => $email,
            "source" => $token,
            'name' => $name_stripe,
            'address' => [
                    'line1' => 'yolo address',
                    'postal_code' => '98140',
                    'city' => 'San Francisco',
                    'state' => 'CA',
                    'country' => 'US',
                  ],
        ));
        
        $error = array();
          // Use Stripe's library to make requests...
        } catch(\Stripe\Exception\CardException $e) {
          // Since it's a decline, \Stripe\Exception\CardException will be caught
          $error = array('ResponseCode' => '0', 'ResponseText' => $e->getError()->message);
        } catch (\Stripe\Exception\RateLimitException $e) {
          // Too many requests made to the API too quickly
                    $error = array('ResponseCode' => '0', 'ResponseText' => $e->getError()->message);
        } catch (\Stripe\Exception\InvalidRequestException $e) {
          // Invalid parameters were supplied to Stripe's API
                    $error = array('ResponseCode' => '0', 'ResponseText' => $e->getError()->message);
        } catch (\Stripe\Exception\AuthenticationException $e) {
          // Authentication with Stripe's API failed
          // (maybe you changed API keys recently)
                    $error = array('ResponseCode' => '0', 'ResponseText' => $e->getError()->message);
        } catch (\Stripe\Exception\ApiConnectionException $e) {
          // Network communication with Stripe failed
                    $error = array('ResponseCode' => '0', 'ResponseText' => $e->getError()->message);
        } catch (\Stripe\Exception\ApiErrorException $e) {
          // Display a very generic error to the user, and maybe send
          // yourself an email
                    $error = array('ResponseCode' => '0', 'ResponseText' => $e->getError()->message);
        } catch (Exception $e) {
              // Something else happened, completely unrelated to Stripe
                $error = array('ResponseCode' => '0', 'ResponseText' => $e->getError()->message);
        }
        if(!empty($error)){
            return $error;
        }
        
       
        // $charge = \Stripe\Charge::create(array(
        //     "amount" => $amount,
        //     "currency" => "usd",
        //     "description" =>  "agency membership",
        //     "customer" => $customer->id
        // ));
        
        // CREATING SUBSCRIPTION
        $stripe = new \Stripe\StripeClient(
          'sk_test_51IL9mZGXQwgSBKzkVsLedeaw474KpbQPOBrXHqhXlOQovxDXIlWdQ9qJK3extBaM5IIOO26LyDAEzvhHKOUSDxZI00HDf7o8Sj'
        );
        $stripe->subscriptions->create([
          'customer' => $customer->id,
          'items' => [
            ['price' => 'price_1IMo73GXQwgSBKzk0t40E5kY'],
          ],
        ]);
        
        
         if($customer){
             $customerdetails = $this->getstripeCusDetails($customer->id);
             
            $addcustomer = DB::table('stripe_customers')
                            ->insert([  
                                'user_id' => $user_id,
                                'stripe_customer_id' => $customerdetails->id,
                                'stripe_default_card' => $customerdetails->default_source,
                                'email' => $customerdetails->email
                            ]); 
        }
        
       $trans = DB::table('membership_transactions')
                ->insert([  
                    'transaction_id' => $transaction_id,
                    'uniquecode' => $uniquecode,
                    'user_id' => $user_id,
                    'email' => $email,
                    'amount' => $amount,
                    'purchase' => 'membership',
                    'ip' => $ip,
                    'month' => $month,
                    'year' => $year,
                ]);
        if($trans){
          $data = DB::table('users')->where('user_id',$user_id)->update(
                [
                    'is_payment' =>  '1',
                    'is_completed' =>  '1',
                ]);
                
                $user = User::where('user_id', $user_id)->first();
                return response()->json([
                    'ResponseCode' => '1',
                    'ResponseText' => 'Payment Succesfull',
                    'token' =>  $this->jwt($user),
                    ]);
          
        }
      }
      public function getstripeCusDetails($customer_id) {
           $stripe = new \Stripe\StripeClient(
              'sk_test_51IL9mZGXQwgSBKzkVsLedeaw474KpbQPOBrXHqhXlOQovxDXIlWdQ9qJK3extBaM5IIOO26LyDAEzvhHKOUSDxZI00HDf7o8Sj'
            );
           $cusdetails = $stripe->customers->retrieve(
              $customer_id,
                 []
            );
            return $cusdetails;
      }
      public function AddNewCard(Request $request) {
            // Add new Card
            
            
             $customer = \Stripe\Customer::create(array(
                    "email" => $email,
                    "source" => $token,
                    'name' => $name_stripe,
                    'address' => [
                            'line1' => 'yolo address',
                            'postal_code' => '98140',
                            'city' => 'San Francisco',
                            'state' => 'CA',
                            'country' => 'US',
                          ],
                ));
            
            
            $customer_id= $request->input('customer_id');
            $token= $request->input('tokenn');
             print_R($token);
            print_R($customer_id); die();
            \Stripe\Stripe::setVerifySslCerts(false);
            
            // $stripe = new \Stripe\StripeClient(
            //   'sk_test_51IL9mZGXQwgSBKzkVsLedeaw474KpbQPOBrXHqhXlOQovxDXIlWdQ9qJK3extBaM5IIOO26LyDAEzvhHKOUSDxZI00HDf7o8Sj'
            // );
            
            $stripe = new \Stripe\StripeClient(
                  'sk_test_51IL9mZGXQwgSBKzkVsLedeaw474KpbQPOBrXHqhXlOQovxDXIlWdQ9qJK3extBaM5IIOO26LyDAEzvhHKOUSDxZI00HDf7o8Sj'
                );
                $stripe->customers->createSource(
                  $customer_id,
                  ['source' => $token]
                );
            
            
              return response()->json([
                    'ResponseCode' => '1',
                    'ResponseText' => 'Card Added Succesfully',
                    ]);
            
            // $stripe->customers->createSource(
            //   $customer_id,
            //   ['source' => $token]
            // );
            
            // $cusdetails = $stripe->customers->retrieve(
            //   $customer_id,
            //      []
            // );
            // print_R($cusdetails);
            // die();
      }
     
}