<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use DB;
use Firebase\JWT\JWT;
// use Twilio\Rest\Client;

use App\Models\User;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
use Stripe\Stripe;
// use Illuminate\Support\Facades\Storage;

class DefaultController extends BaseController {
 private $request;
  
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

protected function jwt(User $user)
    {
       if($user->user_type == 'user'){
            $role = '#U$*43vc4*756y0&78h7$t';
        }else if($user->user_type == 'doctor'){
             $role = '$DoC#79rd*&57h%45@e';
        }else if($user->user_type == 'superadmin'){
             $role = '$$uPU67#$d#77HHJk@8';
        }else if($user->user_type == 'agency'){
             $role = '@GCPU67#$d#FR%%HJYYk@8';
        }else{
             $role = '';
        }
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->user_id, // Subject of the token
            'nik' => $user->name, // nickname of the current user
            'sat' => $user->user_status, // status of the current user
            'urxrs' => $role, // user role
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 1440, // 1209600 //60*60 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

        public function signup(User $user) {
            
             \Stripe\Stripe::setVerifySslCerts(false);
              \Stripe\Stripe::setApiKey('sk_test_51IL9mZGXQwgSBKzkVsLedeaw474KpbQPOBrXHqhXlOQovxDXIlWdQ9qJK3extBaM5IIOO26LyDAEzvhHKOUSDxZI00HDf7o8Sj');

                $validator = Validator::make($this->request->all(), [
                // 'fullname' => 'required',
                'email' => 'required|email|unique:users',
                // 'password' => 'required',
                
                ]);
                $password =  $this->request->input('password');
               
                if ($validator->fails()) {
                    $response = $validator->errors()->first();
                    // return response(json_encode($response));
                    return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => $response
                        ]);
                } else {
        
                    $month = date('m');
                    $year = date("Y");
                    $randnum = rand(1111111111,9999999999);
                    $currtime = date("H:i:s");
                    $currdate = date("Y-m-d");
                    $user_type = 'agency';
                      $phone = $this->request->input('phone');
                    //   $fullname = $this->request->input('fullname');
                   
                   
                       $customer = \Stripe\Customer::create(array(
                            "email" => $this->request->input('email'),
                            'name' => $this->request->input('email'),
                        ));
                   
                   
                   
                    $data = DB::table('users')->insert(
                        [
                            'phone' =>  $phone,
                            'user_id' => $randnum,
                            'reg_date'=>$currdate,
                            'user_type' => $user_type,
                            'email' =>  $this->request->input('email'),
                            'password' => Hash::make($password),
                            'reg_time' => $currtime,
                            'is_personal_info' => '1'
                        ]);
        
                        if($customer){
                            $checkstripecusentries = DB::table('stripe_customers')->select('*')->where('user_id', $randnum)->first();
                            if($checkstripecusentries){
                            }else{
                                  $check = DB::table('stripe_customers')->insert(
                                [
                                    'user_id' =>  $randnum,
                                    'stripe_customer_id' => $customer->id,
                                    'stripe_default_card'=>'0',
                                    'email' =>  $this->request->input('email'),
                                ]);
                            }
                        }
        
                        if ($data) {
                            
                            
                            $from = 'info@usahealthmd.com';
                            $message = '<div class="" style="float:none;width:650px;margin:auto;">
	<div style="float: left;width: 100%;background: #f8f8f8; padding: 0 0px;">
		<p style="width:100%;float:left;height: 50px;background: #1eb7e4;margin: 0 0;"></p>
	     <table style=" width: 100%;float: left;text-align: center;">
			<tbody>
				<tr style=" width: 100%;float: left;"><td style=" width: 100%;float: left;margin-top: 22px;"><img style="width: 200px;margin-top: -62px;" src="https://www.usahealthmd.com/assets/images/logo.png"></td>
				</tr>
			   </tbody>
		      </table>
		
		
		<table style="float: left;width: 100%;margin-top: 2px;padding: 0 12px;">
		<thead style="float: left;width: 100%;">
	         <tr style="float: left;width: 100%;">
				<td style="float: left;width: 100%;text-align: center;"><h2 style="color: #1eb7e4;font-size: 24px;">Welcome to USA Health MD</h2></td>
				<td style="float: left;width: 100%;"><p style="float: left;width: 100%;margin:0px;text-align: center;color: #575454;font-size: 20px;line-height: 30px;">Thank you for signing up on USA Health MD, USA Health MD is first of its kind digital marketing plateform, that brings together private pay medical practices with consumers searching for cutting-edge solutions and treatments in their local geographic area and throughout the U.S. </p></td>
			</tr>
	    <tr style="float: left;width: 100%;"><td style="float: left;width: 100%;text-align: center;"><img style="width: 270px;margin: 20px 0;" src="https://www.usahealthmd.com/assets/images/Doctor/USA-Health-MD-Doctors.png"></td>
	   </tr>
	  </thead>
	 </table>

	 <table style="float: left;width: 100%;margin-top: 2px;padding: 0 4px;">
		<thead style="float: left;width: 100%;">
	    <tr style="float: left;width: 100%;">
		<td style="float: left;width: 100%;text-align: center;"><h2 style="color: #1eb7e4;font-size: 24px; margin-top: 0px;">Robust Growth Tools In Your Hand!</h2></td>
		<td style="float: left;width: 100%;"><p style="float: left;width: 100%;margin:0px;text-align: center;color: #575454;font-size: 20px;line-height: 30px;">Our powerful practice search engine, coupled with our comprehensive marketing tools and strategies, assures that your practice will be found by the target audience that you want to treat.</p></td>
        <td style="float: left;width: 100%; text-align: center;margin-top: 30px;padding-bottom: 30px;"><a target="_blank" style="background: #1eb7e4;color: #ffffff;text-align: center;padding: 12px 26px;font-size: 22px;cursor: pointer;text-decoration: none;border-radius: 6px;" href="https://www.usahealthmd.com/doctors">Learn More</a></td>
	 </tr>
    </thead>
   </table>

	</div>
	</div>';
                        	$useremail = $this->request->input('email');
                            $email = new \SendGrid\Mail\Mail();
                            $email->setFrom("$from", "USA Health MD");
                            $email->setSubject("Welcome To USA Health MD");
                            $email->addTo($useremail, 'USA Health MD');
                            $email->addContent("text/plain", $message);
                            $email->addContent("text/html", $message);
                            $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
                            $sendgrid->send($email);
                
                
                             $userr = DB::table('users')->select('*')->where('user_id', $randnum)->first();
                             $userdata = array('userid'=>$userr->user_id,'user_role'=>$userr->user_type,'email'=>$userr->email,'is_personal_info'=>$userr->is_personal_info,'is_professional_info'=>$userr->is_professional_info,'is_plan'=>$userr->is_plan,'is_speciality_info'=>$userr->is_speciality_info,'is_status'=>$userr->is_status,'is_payment'=>$userr->is_payment,'is_completed'=>$userr->is_completed);
                           
                            return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Registration Successfully',
                                'token' => $this->jwt($user),
                                'data' => $userdata
                            ], 200);
                             
                        } else {
                            return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => 'Registration UnSuccessfull.'
                            ]);
                        }
                    }
        }
        
          public function checkPhone(){
            $phone = $this->request->input('phone');
            $email = $this->request->input('email');
            $checkemail = DB::table('users')->select('*')->where('email', $email)->first();
            if($checkemail){
                    return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => 'Email is already taken.'
                            ]);
             }
             $userr = DB::table('users')->select('*')->where('phone', $phone)->first();
             if($userr){
                    return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => 'Phone number is already taken.'
                            ]);
             }
            $code = rand(100000,1000000);
              try {
                            $account_sid = getenv("TWILIO_SID");
                            $auth_token = getenv("TWILIO_AUTH_TOKEN");
                            $twilio_number = getenv("TWILIO_NUMBER");
                            // $phone = $request->input('phone');
                          $message = "Your One time Verification Code is ".$code ." For US Health MD. please Confirm your number";
                            $correctmessage = str_replace("%20"," ",$message);
                            $client = new Client($account_sid, $auth_token);
                            $gg = $client->messages->create($phone, 
                            ['from' => $twilio_number, 'body' => $correctmessage] );
                           
                           return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'done',
                                'code' => $code
                            ], 200);
                           
                            } catch (\Exception $e){
                                // print_r($e);
                               
                						if($e->getCode() == 21211)
                						{
                						  //  print_r($e);
                						  return response()->json([
                                            'ResponseCode' => '0',
                                            'ResponseText' => 'Please enter a valid phone number',
                                        ], 200);
                						}
                						 return response()->json([
                                            'ResponseCode' => '0',
                                            'ResponseText' => 'error',
                                        ], 200);
                			}
          }
            public function regOne(User $user) {
     
                    $user_id = $this->request->input('user_id');
                    $state = $this->request->input('state');
                    $address = $this->request->input('address');
                    $city = $this->request->input('city');
                    $firstname = $this->request->input('firstname');
                    $profilepic = md5(rand(11111, 999990) . date('Ymdhis')) . "." .  $this->request->file('profilepic')->getClientOriginalExtension();
                     $path = getcwd();
                     $dir = $path.'/profilpictures';
                      if (!is_dir($dir)) {
                            mkdir($dir);
                        }
                             $data = DB::table('users')->where('user_id',$user_id)->update(
                                [
                                    'name' =>  $firstname,
                                    'address'=> $address,
                                    'state' => $state,
                                    'profilepic' => $profilepic,
                                    'city'=>$city
                                ]);
                           
        
                        if ($data) {
                             $this->request->file('profilepic')->move("$dir", $profilepic);
                             $data = DB::table('users')->where('user_id',$user_id)->update(
                                [
                                    'is_personal_info' =>  '1',
                                ]);
                         $userr = DB::table('users')->select('*')->where('user_id', $user_id)->first();
                          $userdata = array('userid'=>$userr->user_id,'user_role'=>$userr->user_type,'email'=>$userr->email,'is_personal_info'=>$userr->is_personal_info,'is_professional_info'=>$userr->is_professional_info,'is_plan'=>$userr->is_plan,'is_speciality_info'=>$userr->is_speciality_info,'is_status'=>$userr->is_status,'is_payment'=>$userr->is_payment,'is_completed'=>$userr->is_completed,);
                            return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Ist Step completed',
                                'data' => $userdata
                            ], 200);
                           
                        } else {
                            return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => 'Registration UnSuccessfull.'
                            ]);
                        }
                    // }
        }
        
          public function regTwo(User $user) {
                $validator = Validator::make($this->request->all(), [
                'hospital_address' => 'required',
                 'hospital_name' => 'required',
                'hospital_state' => 'required',
                'hospital_city' =>'required',
                ]);
              
                if ($validator->fails()) {
                    $response = $validator->errors()->first();
                    return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => $response
                        ]);
                } else {
        
                    $user_id = $this->request->input('user_id');
                    
                     $hospital_name = $this->request->input('hospital_name');
                    $hospital_address = $this->request->input('hospital_address');
                    $hospital_state = $this->request->input('hospital_state');
                    $hospital_city = $this->request->input('hospital_city');
                    
                    
                    // print_r($hospital_name);  print_r($hospital_address);  print_r($hospital_state);  print_r($hospital_city); print_r($user_id); die();
                        $data = DB::table('users')->where('user_id',$user_id)->update(
                                [
                                    'hospital_name' =>  $hospital_name,
                                    'hospital_address'=> $hospital_address,
                                    'hospital_state' => $hospital_state,
                                    'hospital_city' => $hospital_city,
                                ]);
                        if ($data) {
                             $data = DB::table('users')->where('user_id',$user_id)->update(
                                [
                                    'is_professional_info' =>  '1',
                                    //  'is_speciality_info' =>  '1',
                                    //  'is_status'=> '1'
                                ]);
                         $userr = DB::table('users')->select('*')->where('user_id', $user_id)->first();
                          $userdata = array('userid'=>$userr->user_id,'user_role'=>$userr->user_type,'email'=>$userr->email,'is_personal_info'=>$userr->is_personal_info,'is_professional_info'=>$userr->is_professional_info,'is_plan'=>$userr->is_plan,'is_speciality_info'=>$userr->is_speciality_info,'is_status'=>$userr->is_status,'is_payment'=>$userr->is_payment,'is_completed'=>$userr->is_completed,);
                            return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => '2nd Step completed',
                                'data' => $userdata
                            ], 200);
                           
                        } else {
                            return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => 'Registration UnSuccessfull.'
                            ]);
                        }
                    
                    
                }
        }
        
          public function regChosePlan(User $user) {
                    $user_id = $this->request->input('user_id');
                    $plan_id = $this->request->input('plan_id');
                    
                    $plan_details = DB::table('user_plans')->where('uniquecode',$plan_id)->first();
                    $planlimit = $plan_details->plan_limit;
                    $data = DB::table('users')->where('user_id',$user_id)->update(
                                    [
                                        'plan_id' =>  $plan_id,
                                        'plan_limit' => $planlimit
                                    ]);
               
                    $up =  DB::table('users')->where('user_id',$user_id)->update(
                        [
                            'is_plan'=> '1',
                        ]);
                //  if ($up) {
                 $userr = DB::table('users')->select('*')->where('user_id', $user_id)->first();
                  $userdata = array('userid'=>$userr->user_id,'user_role'=>$userr->user_type,'email'=>$userr->email,'is_personal_info'=>$userr->is_personal_info,'is_professional_info'=>$userr->is_professional_info,'is_plan'=>$userr->is_plan,'is_speciality_info'=>$userr->is_speciality_info,'is_status'=>$userr->is_status,'is_payment'=>$userr->is_payment,'is_completed'=>$userr->is_completed,);
                    return response()->json([
                        'ResponseCode' => '1',
                        'ResponseText' => 'You successfully Choose the plan',
                        'data' => $userdata
                    ], 200);
                   
                // } else {
                //     return response()->json([
                //     'ResponseCode' => '0',
                //     'ResponseText' => 'Registration UnSuccessfull.'
                //     ]);
                // }    
                    
          }
          
          
           public function base64_to_jpeg($base64_string, $output_file) {
                // open the output file for writing
                $ifp = fopen( $output_file, 'wb' ); 
            
                $data = explode( ',', $base64_string );
            
                // we could add validation here with ensuring count( $data ) > 1
                fwrite( $ifp, base64_decode( $data[ 1 ] ) );
            
                // clean up the file resource
                fclose( $ifp ); 
            
                return $output_file; 
            }
         public function proffessionalData(User $user) {
     
                $validator = Validator::make($this->request->all(), [
                'hospital_address' => 'required',
                'hospital_name' => 'required',
                'hospital_state' => 'required',
                'hospital_city' =>'required',
                'hospital_phone' =>'required',
                'hospital_email' =>'required',
                'hospital_website' =>'required',
                ]);
              
                if ($validator->fails()) {
                    $response = $validator->errors()->first();
                    return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => $response
                        ]);
                } else {
        
                $user_id = $this->request->input('user_id');
                    
                $hospital_name = $this->request->input('hospital_name');
                $hospital_address = $this->request->input('hospital_address');
                $hospital_state = $this->request->input('hospital_state');
                $hospital_city = $this->request->input('hospital_city');
                $hospital_phone = $this->request->input('hospital_phone');
                $hospital_email = $this->request->input('hospital_email');
                $hospital_website = $this->request->input('hospital_website');
                $hospital_overview = $this->request->input('hospital_overview');
                $lat = $this->request->input('hosp_latitude');
                $long = $this->request->input('hosp_longitude');
                
                if($this->request->input('is_financing')){
                    $is_financing = $this->request->input('is_financing');
                }else{
                    $is_financing = '0';
                }
                
                
                
                $hospital_logo_base = $this->request->input('hospital_logo_base');
                $hospital_banner_base = $this->request->input('hospital_banner_base');
                 
                 
                //   if($this->request->file('hospital_logo')){
                //         $validator_hospital_logo = Validator::make($this->request->all(), [
                //             'hospital_logo' => 'max:1024',
                //         ]);
                //         if($validator_hospital_logo->fails()){
                //              $response = $validator_hospital_logo->errors()->first();
                //                 return response()->json([
                //                         'ResponseCode' => '0',
                //                         'ResponseText' => 'The hospital logo may not be greater than 1 MB.'
                //                     ]);
                //         }
                //           $hospital_logo = md5(rand(11111, 999990) . date('Ymdhis')) . "." .  $this->request->file('hospital_logo')->getClientOriginalExtension();
                //           $path = getcwd();
                //              $dir = $path.'/hospitalimages';
                //               if (!is_dir($dir)) {
                //                     mkdir($dir);
                //         }
                //             if($this->request->file('hospital_logo')){
                //                  $this->request->file('hospital_logo')->move("$dir", $hospital_logo);
                //             }
                //     }else{
                //         $hospital_logo = 'null';
                //     }
                 
                // if($this->request->file('hospital_banner')){
                //      $validator_hospital_banner = Validator::make($this->request->all(), [
                //         'hospital_banner' => 'max:1024',
                //     ]);
                //     if($validator_hospital_banner->fails()){
                //          $response = $validator_hospital_banner->errors()->first();
                //             return response()->json([
                //                     'ResponseCode' => '0',
                //                     'ResponseText' => 'The hospital Banner may not be greater than 1 MB.'
                //                 ]);
                //     }
                //       $hospital_banner = md5(rand(11111, 999990) . date('Ymdhis')) . "." .  $this->request->file('hospital_banner')->getClientOriginalExtension();
                //       if($this->request->file('hospital_banner')){
                //             $this->request->file('hospital_banner')->move("$dir", $hospital_banner);
                //         }
                // }else{
                //     $hospital_banner = 'null';
                // }
                if($hospital_overview){
                    $hospitaloverview = $hospital_overview;
                }else{
                    $hospitaloverview = 'null';
                }
                
                if($hospital_logo_base){
                    $name = str_replace(' ', '+', $hospital_logo_base);
                    $imageName = rand(11111, 999990) . '.png';
                    $path = getcwd();
                    $dir = $path.'/hospitalimages/'.$imageName;
                    $image = $this->base64_to_jpeg( $name, $dir ); 
                    $hospital_logo = $imageName;
                }
                    
                if($hospital_banner_base){
                    $namee = str_replace(' ', '+', $hospital_banner_base);
                    $imageNamee = rand(111111, 9999990) . '.png';
                    $pathh = getcwd();
                    $dirr = $pathh.'/hospitalimages/'.$imageNamee;
                    $imagee = $this->base64_to_jpeg( $namee, $dirr );
                    $hospital_banner = $imageNamee;
                }
                
                $data = DB::table('users')->where('user_id',$user_id)->update(
                        [
                            'hospital_name' =>  $hospital_name,
                            'hospital_address'=> $hospital_address,
                            'hospital_state' => $hospital_state,
                            'hospital_city' => $hospital_city,
                            'hospital_phone' => $hospital_phone,
                            'hospital_email' => $hospital_email,
                            'hospital_website'=> $hospital_website,
                            'hospital_overview' => $hospitaloverview,
                            'hospital_latitude' => $lat,
                            'hospital_longitude' => $long,
                            'is_financing' => $is_financing
                            ]);
                             if($hospital_logo_base){
                                  $data = DB::table('users')->where('user_id',$user_id)->update(
                                    [
                                        'hospital_logo' => $hospital_logo,
                                        ]);
                             }
                              if($hospital_banner_base){
                                  $data = DB::table('users')->where('user_id',$user_id)->update(
                                    [
                                        'hospital_banner' => $hospital_banner,
                                        ]);
                             }
                     $data = DB::table('users')->where('user_id',$user_id)->update(
                        [
                            'is_professional_info' =>  '1',
                            'is_practice_introjs' =>  '1',
                            //  'is_speciality_info' =>  '1',
                            //  'is_status'=> '1'
                        ]);
                $userr = DB::table('users')->select('*')->where('user_id', $user_id)->first();
                $userdata = array('userid'=>$userr->user_id,'user_role'=>$userr->user_type,'email'=>$userr->email,'is_personal_info'=>$userr->is_personal_info,'is_professional_info'=>$userr->is_professional_info,'is_plan'=>$userr->is_plan,'is_speciality_info'=>$userr->is_speciality_info,'is_status'=>$userr->is_status,'is_payment'=>$userr->is_payment,'is_completed'=>$userr->is_completed,);
                    return response()->json([
                        'ResponseCode' => '1',
                        'ResponseText' => 'Profile Updated Successfull',
                        'data' => $userdata
                    ], 200);
                    
            }
        }
        
            public function specialityData(User $user) {
     
                $validator = Validator::make($this->request->all(), [
                'doc_speciality' => 'required',
                // 'doc_subcategory' => 'required',
                // 'doc_treatments' => 'required',
                // 'doc_treatmentsoverview' =>'required',
                ]);
              
                if ($validator->fails()) {
                    $response = $validator->errors()->first();
                    return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => $response
                        ]);
                } else {
        
                    $user_id = $this->request->input('user_id');
                    
                     $doc_speciality = $this->request->input('doc_speciality');
                    $doc_subcategory = $this->request->input('doc_subcategory');
                    // $doc_treatments = $this->request->input('doc_treatments');
                    // $doc_treatmentsoverview = $this->request->input('doc_treatmentsoverview');
                    
                     $array = explode(',', $doc_speciality);
                    $cateee = DB::table('speciality')->select('speciality_id')->whereIn('speciality_name',$array)->groupby('speciality_id')->get();
                   
                    foreach($cateee as $key=>$value)
                    {
                        if($value->speciality_id){
                            $dt= $value->speciality_id;
                            $temp[]=$dt;
                        }else{
                            
                        }
                    }
                    $splids = implode(",",$temp);
                  
                    // print_r($splids); die();
                        $data = DB::table('users')->where('user_id',$user_id)->update(
                                [
                                    'doc_speciality' =>  $splids,
                                    'doc_subcategory'=> $doc_subcategory,
                                    // 'doc_treatments' => $doc_treatments,
                                    // 'doc_treatmentsoverview' => $doc_treatmentsoverview,
                                    ]);
                        // if ($data) {
                            $data = DB::table('users')->where('user_id',$user_id)->update(
                                [
                                    'is_speciality_info' =>  '1',
                                ]);
                            $userr = DB::table('users')->select('*')->where('user_id', $user_id)->first();
                          $userdata = array('userid'=>$userr->user_id,'user_role'=>$userr->user_type,'email'=>$userr->email,'is_personal_info'=>$userr->is_personal_info,'is_professional_info'=>$userr->is_professional_info,'is_plan'=>$userr->is_plan,'is_speciality_info'=>$userr->is_speciality_info,'is_status'=>$userr->is_status,'is_payment'=>$userr->is_payment,'is_completed'=>$userr->is_completed,);
                            return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'completed',
                                'data' => $userdata
                            ], 200);
                           
                        // } else {
                        //     return response()->json([
                        //     'ResponseCode' => '0',
                        //     'ResponseText' => 'Registration UnSuccessfull.'
                        //     ]);
                        // }
                }
        }
        
}