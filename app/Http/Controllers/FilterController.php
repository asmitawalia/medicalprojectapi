<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class FilterController extends BaseController {
    


   public function getstatesdata(Request $request) {
            
            $user = DB::table('filters')->first();
            // print_R($user->location);
            $location = json_decode($user->location);
            function isJson($location) {
             json_decode($location);
             return (json_last_error() == JSON_ERROR_NONE);
            }
            if($user){
                return response()->json([
                     'ResponseCode' => '1',
                        'Data' => $location,
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '0',
                    'ResponseText' => 'error try again',
                ]);
            }
    }
     public function getCitydata(Request $request,$id) {
            $user = DB::table('cities')->where('state',$id)->first();
            if($user){
                $location = $user->cities;
                $array = json_decode($location);
            }else{
                $array =  array();
            }
            // print_r($user);
                return response()->json([
                     'ResponseCode' => '1',
                        'Cities' => $array,
                ]);
          
    }
}
