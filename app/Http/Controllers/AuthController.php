<?php
namespace App\Http\Controllers;
use DB;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
use Validator;
use Carbon\Carbon;
use Twilio\Rest\Client;

class AuthController extends BaseController
{
    private $request;
  
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    protected function jwt(User $user)
    {
        if($user->user_type == 'user'){
            $role = '#U$*43vc4*756y0&78h7$t';
        }else if($user->user_type == 'doctor'){
             $role = '$DoC#79rd*&57h%45@e';
        }else if($user->user_type == 'superadmin'){
             $role = '$$uPU67#$d#77HHJk@8';
        }else if($user->user_type == 'agency'){
             $role = '@GCPU67#$d#FR%%HJYYk@8';
        }else{
             $role = '';
        }
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->user_id, // Subject of the token
            'nik' => $user->email, // nickname of the current user
            'sat' => $user->user_status, // status of the current user
            'completed'=> (int)$user->is_completed,
            'urxrs' => $role, // user role
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 1440*365, // 1209600 //60*60 // Expiration time
            // 'exp' => 43800
        ];
        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }
    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param  \App\User   $user
     * @return mixed
     */
    public function authenticate(User $user)
    {
      
         $validator = Validator::make($this->request->all(), [
                'email' => 'required|email',
                'password' => 'required',
                ]);
              
              if ($validator->fails()) {
                    $response = $validator->errors()->first();
                    // return response(json_encode($response));
                    return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => $response
                        ]);
                } 
              
               
        // Find the user by email
        $user = User::where('email', $this->request->input('email'))->first();
       
        if (!$user) {

            return response()->json([
                'ResponseCode' => '0',
                'ResponseText' => 'This user Does not Exist'
                
            ], 400);
        }
        if (Hash::check($this->request->input('password'), $user->password)) {
              $afterfcmuser = DB::table('users')->select('*')->where('user_id', $user->user_id)->first();
             $userdata = array('userid'=>$user->user_id,'is_personal_info'=>$user->is_personal_info,'is_professional_info'=>$user->is_professional_info,'is_speciality_info'=>$user->is_speciality_info,'is_plan'=>$user->is_plan,'is_status'=>$user->is_status,'is_payment'=>$user->is_payment,'is_completed'=>$user->is_completed,'name'=>$user->name,'email'=>$user->email,'phone'=>$user->phone,'type'=>$user->user_type);
            return response()->json([
                'ResponseCode' => '1',
                'ResponseText' => 'Login Successfully',
                'token' => $this->jwt($user),
                'data' => $userdata
            ], 200);
        }else{
            return response()->json([
                'ResponseCode' => '0',
                'ResponseText' => 'The Password is Incorrect'
                
            ], 400);
        }
    }

    
}
