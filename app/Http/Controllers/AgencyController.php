<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class AgencyController extends BaseController {
     public function getAgencies(Request $request,$id) {
         if($id){
            $skip = $id*10;
        }else{
             $skip = $id*10;
        }
        $limit = '10';
          $user = DB::table('users')->select('*')->where([['is_completed','1'],['is_status','1'],['user_type','agency'],['user_status','active']])->skip($skip)->take($limit)->get();
          $agencysdata =  json_decode(json_encode($user), TRUE);
          if($user){
            foreach ($agencysdata as $key => &$value) {
                $splid = $value['doc_speciality'];
                $splidarray =  explode(",",$splid);
                        $splsnames = DB::table('speciality')->whereIn('speciality_id',$splidarray)->where('speciality_subcategory_id','0')->pluck('speciality_name');
                  
                   
                    if($splsnames){
                         $value['spl_names'] = $splsnames;
                    }else{
                         $value['spl_names'] = array();
                    }
                   
               
            }
          }
        
          $count = DB::table('users')->select('*')->where([['is_completed','1'],['user_type','agency']])->count();
            $array = array('data'=> $agencysdata,'count'=>$count);
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $array,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $array,
                    ]);
                }
     }
    public function getAgenciesbyCity(Request $request,$slug,$city,$page) {
         if($page){
            $skip = $page*10;
        }else{
             $skip = $page*10;
        }
        $limit = '10';
        
        // $user = DB::table('cities')->where('state',$id)->first();
        // $location = $user->cities;
        // $array = json_decode($location);
        
        
        
        
         $cateee = DB::table('speciality')->select('speciality_id')->where('slug',$slug)->first();
        $idd = $cateee->speciality_id;
        
        $firstuser = DB::table('users')->select('*')->where([['is_completed','1'],['is_status','1'],['user_type','agency'],['hospital_city',$city],['user_status','active']])->whereRaw('FIND_IN_SET(?,doc_speciality)', [$idd])->first();
         if($firstuser){
                $hosplat = $firstuser->hospital_latitude;
                $hosplong = $firstuser->hospital_longitude;
         }else{
             $hosplat = '';
                $hosplong = '';
         }
        
        
          $user = DB::table('users')->select('*')->where([['is_completed','1'],['is_status','1'],['user_type','agency'],['hospital_city',$city]])->whereRaw('FIND_IN_SET(?,doc_speciality)', [$idd])->skip($skip)->take($limit)->get();
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                                'lat' =>$hosplat,
                                'long' =>$hosplong,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                                'lat' =>$lat,
                                    'long' =>$long,
                    ]);
                }
     }
     
     
     
       public function getAgenciesbyState(Request $request,$slug,$state,$page) {
             if($page){
                $skip = $page*10;
            }else{
                 $skip = $page*10;
            }
            $limit = '10';
            
            $allStates = DB::table('filters')->first();
            $location = json_decode($allStates->location);
            
                foreach ($location as $key => $value) {
                    if($value->slug == $state){
                        $lat = $value->latitude;
                         $long = $value->longitude;
                    }
                }
            $cateee = DB::table('speciality')->select('speciality_id')->where('slug',$slug)->first();
            $idd = $cateee->speciality_id;
            
            
            
              $user = DB::table('users')->select('*')->where([['is_completed','1'],['is_status','1'],['user_type','agency'],['hospital_state',$state],['user_status','active']])->whereRaw('FIND_IN_SET(?,doc_speciality)', [$idd])->skip($skip)->take($limit)->get();
                    if($user){
                         return response()->json([
                              'ResponseCode' => '1',
                                    'data' => $user,
                                    'lat' =>$lat,
                                    'long' =>$long,
                        ]);
                    }else{
                          return response()->json([
                              'ResponseCode' => '1',
                                    'data' => $user,
                                    'lat' =>'',
                                    'long' =>'',
                        ]);
                    }
         }
     
     
     
         public function getSingleAgency(Request $request,$id) {
          $user = DB::table('users')->select('*')->where('user_id',$id)->first();
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }
     }
     
      public function getSpecificAgencies(Request $request) {
        
          $user = DB::table('users')->select('*')->where([['is_completed','1'],['is_status','1'],['user_type','agency'],['user_status','active']])->get();
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }
     }
     
     
     
                  public function base64_to_jpeg($base64_string, $output_file) {
                    // open the output file for writing
                    $ifp = fopen( $output_file, 'wb' ); 
                
                    $data = explode( ',', $base64_string );
                
                    // we could add validation here with ensuring count( $data ) > 1
                    fwrite( $ifp, base64_decode( $data[ 1 ] ) );
                
                    // clean up the file resource
                    fclose( $ifp ); 
                
                    return $output_file; 
                }
     
     
     
     
      public function addDoctor(Request $request) {
          $agency_id = $request->input('agency_id');
          $doc_id = rand(11111, 999990);
          $nameee = $request->input('name');
          $email =  $request->input('email');
          $speciality = $request->input('speciality');
          $propicc = $request->input('propic');
        //   if($request->file('propic'))
        //   {
        //       $propic =  md5(rand(11111, 999990) . date('Ymdhis')) . "." . $request->file('propic')->getClientOriginalExtension();
        //   }else{
        //       $propic =  '';
        //   }
          
            if($propicc){
                    $name = str_replace(' ', '+', $propicc);
                    $imageName = rand(1111111, 99999990) . '.png';
                    $path = getcwd();
                    $dir = $path.'/doctors/'.$imageName;
                    $image = $this->base64_to_jpeg( $name, $dir ); 
                    $propic = $imageName;
                }else{
                     $propic = '';
                }
          $checkCount =   DB::table('doctors')->select('*')->where([['agency_id',$agency_id],['status','active']])->count();
          if($checkCount >= 2){
               return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'You Cannot add more than 2 Doctors.',
                    ],200);
          }
          
          
           $check =   DB::table('doctors')->select('*')->where([['email',$email],['status','active']])->first();
           if($check){
                return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Email is Already Taken.',
                    ],200);
           }
            $up =   DB::table('doctors')
            ->insert([  
                            'name' => $nameee,
                            'email' => $email,
                            'speciality' => $speciality,
                            // 'propic' => $propic,
                            'doc_id' => $doc_id     ,
                            'agency_id' => $agency_id
                       
                       ]);
            if($propicc){
                      DB::table('doctors')->where('doc_id',$doc_id)->update(
                        [
                            'propic' => $propic,
                            ]);
                 }
         if($up){
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Doctor Added Successfully.',
                    ],200);
         }else{
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
         } 
        }
        
        public function updateDoctor(Request $request) {
          $doc_id = $request->input('doc_id');
          $nameee = $request->input('name');
          $speciality = $request->input('speciality');
          
          $propicc = $request->input('propic');
           if($propicc){
                $name = str_replace(' ', '+', $propicc);
                $imageName = rand(1111111, 99999990) . '.png';
                $path = getcwd();
                $dir = $path.'/doctors/'.$imageName;
                $image = $this->base64_to_jpeg( $name, $dir ); 
                $propic = $imageName;
            }else{
                 $propic = '';
            }
         
            $up =   DB::table('doctors')->where('doc_id',$doc_id)->update([  
                            'name' => $nameee,
                            'speciality' => $speciality,
                       ]);
                       
                if($propicc){
                      DB::table('doctors')->where('doc_id',$doc_id)->update(
                        [
                            'propic' => $propic,
                        ]);
                 } 
         if($up){
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Doctor Updated Successfully.',
                    ],200);
         }else{
              return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Doctor Updated Successfully.',
                    ],200);
         } 
        }
        
        
        
        
        
        
        
        
          public function getDoctors(Request $request,$id) {
              $docs =   DB::table('doctors')->select('*')->where([['agency_id',$id],['status','active']])->get();
                return response()->json([
                          'ResponseCode' => '1',
                                'data' => $docs,
                    ]);
          }
          
          public function deleteDoctor(Request $request,$id) {
              $up =   DB::table('doctors')->where('doc_id',$id)->delete();
           
               if($up){
                     return response()->json([
                                        'ResponseCode' => '1',
                                        'ResponseText' => 'Doc Deleted Successfully.',
                            ],200);
                 }else{
                     return response()->json([
                                        'ResponseCode' => '0',
                                        'ResponseText' => 'Error Occured, please try again.',
                            ],400);
                 } 
          }
         public function getAgenciesWithCategory(Request $request) {
         $cateee = DB::table('speciality')->select('*')->get();
         foreach($cateee as $key=>$value)
                    {
                        if($value->tags){
                            $dt= $value->tags;
                            $value->decodedtags = json_decode($dt);
                        }else{
                             $value->decodedtags = array();
                        }
                    }
             if($cateee){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }
         }
            public function getParticularAgencies(Request $request,$id) {
                $spl_id =  $request->input('spl_id');
                $sub_spl_id =  $request->input('sub_spl_id');
                $lat =  $request->input('lat');
                $lon =  $request->input('long');
                // $lat = '31.610080';
                // $lon = '75.090524';
                
                // $lat = '31.354635';
                // $lon = '75.570326';
                    if($id){
                        $skip = $id*10;
                    }else{
                         $skip = $id*10;
                    }
                    $max_distance = 30;
                    $limit = '10';
                                if($spl_id == "0" && $sub_spl_id == "0"){
                                     $user = DB::table('users')->select('*',DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                                    * cos(radians(hospital_latitude)) 
                                    * cos(radians(hospital_longitude) - radians(" . $lon . ")) 
                                    + sin(radians(" .$lat. ")) 
                                    * sin(radians(hospital_latitude))) AS distance"))->where(DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                                    * cos(radians(hospital_latitude)) 
                                    * cos(radians(hospital_longitude) - radians(" . $lon . ")) 
                                    + sin(radians(" .$lat. ")) 
                                    * sin(radians(hospital_latitude)))") ,'<', $max_distance)->where([['is_completed','1'],['user_type','agency']])->skip($skip)->take($limit)->get();
                                    
                                     return response()->json([
                                      'ResponseCode' => '1',
                                            'data' => $user,
                                    ]);
                                }
                if($sub_spl_id != '0')
                {
                     $user = DB::table('users')->select('*',DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                        * cos(radians(hospital_latitude)) 
                        * cos(radians(hospital_longitude) - radians(" . $lon . ")) 
                        + sin(radians(" .$lat. ")) 
                        * sin(radians(hospital_latitude))) AS distance"))->whereRaw('FIND_IN_SET(?,doc_speciality)', [$spl_id])->orwhereRaw('FIND_IN_SET(?,doc_subcategory)', [$sub_spl_id])->where(DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                        * cos(radians(hospital_latitude)) 
                        * cos(radians(hospital_longitude) - radians(" . $lon . ")) 
                        + sin(radians(" .$lat. ")) 
                        * sin(radians(hospital_latitude)))") ,'<', $max_distance)->where([['is_completed','1'],['user_type','agency']])->skip($skip)->take($limit)->get();
                }else{
                    // $user = DB::table('users')->whereRaw('FIND_IN_SET(?,doc_speciality)', [$spl_id])->skip($skip)->take($limit)->get();
                    
                    //  $user = DB::table('users')->select('*',DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                    //     * cos(radians(hospital_latitude)) 
                    //     * cos(radians(hospital_longitude) - radians(" . $lon . ")) 
                    //     + sin(radians(" .$lat. ")) 
                    //     * sin(radians(hospital_latitude))) AS distance"))->whereRaw('FIND_IN_SET(?,doc_speciality)', [$spl_id])->get();
                         $user = DB::table('users')->select('*',DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                        * cos(radians(hospital_latitude)) 
                        * cos(radians(hospital_longitude) - radians(" . $lon . ")) 
                        + sin(radians(" .$lat. ")) 
                        * sin(radians(hospital_latitude))) AS distance"))->whereRaw('FIND_IN_SET(?,doc_speciality)', [$spl_id])->where(DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                        * cos(radians(hospital_latitude)) 
                        * cos(radians(hospital_longitude) - radians(" . $lon . ")) 
                        + sin(radians(" .$lat. ")) 
                        * sin(radians(hospital_latitude)))") ,'<', $max_distance)->where([['is_completed','1'],['user_type','agency']])->get();
                }
                
               
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }
            }
    public function updateCustomer(Request $request) {
        $req = $request->all();
        echo '<pre>';
        echo 'herrererer';
        print_r($req);
        die();
    }
         
}