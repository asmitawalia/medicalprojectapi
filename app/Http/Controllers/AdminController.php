<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class AdminController extends BaseController {
    
public function aproovedUsers(Request $request) {
            
             $data = DB::table('users')->select('*')->where([['is_status','1'],['user_type','agency'],['user_status','active']])->get();
            if($data){
                return response()->json([
                     'ResponseCode' => '1',
                        'Data' => $data,
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '1',
                        'Data' => $data,
                ]);
            }
    }
    public function NotaproovedUsers(Request $request) {
            
             $data = DB::table('users')->select('*')->where([['is_speciality_info','1'],['is_status','0'],['user_type','agency'],['user_status','inactive']])->get();
            if($data){
                return response()->json([
                     'ResponseCode' => '1',
                        'Data' => $data,
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '1',
                        'Data' => $data,
                ]);
            }
    }

   public function aprooveStatus(Request $request,$id) {
            $user = DB::table('users')->select('*')->where('user_id',$id)->first();
            $phone = $user->phone;
            $data = DB::table('users')->where('user_id',$id)->update(
                                [
                                    'is_status' =>  '1',
                                    'user_status' => 'active'
                                ]);
            try {
                $account_sid = getenv("TWILIO_SID");
                $auth_token = getenv("TWILIO_AUTH_TOKEN");
                $twilio_number = getenv("TWILIO_NUMBER");
                $link = 'https://www.medicalmarketingsolutionprojects.com/login';
                // $phone = $request->input('phone');
              $message = "Your US Health MD profile is aproved by the admin click here to login ".$link;
                $correctmessage = str_replace("%20"," ",$message);
                $client = new Client($account_sid, $auth_token);
                $gg = $client->messages->create($phone, 
                ['from' => $twilio_number, 'body' => $correctmessage] );
          
                } catch (\Exception $e){
    						if($e->getCode() == 21211)
    						{
    						}
    			}
            if($data){
                return response()->json([
                     'ResponseCode' => '1',
                        'ResponseText' => 'approoved',
                ]);
            }else{
                return response()->json([
                      'ResponseCode' => '1',
                        'ResponseText' => 'approoved',
                ]);
            }
    }
    public function addSpeciality(Request $request) {
        $spl_name =  $request->input('spl_name');
        $checkexistance = DB::table('speciality')->select('*')->where('speciality_name',$spl_name)->first();
        if($checkexistance){
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'This Speciality is already there.',
                    ],200);
        }
        $slug = preg_replace('#[ -]+#', '-', $spl_name);
        $sub_spl_name =  $request->input('sub_spl_name');
        $tags=array();
        array_push($tags,$spl_name);
        $decodedtags = json_encode($tags);
        $rand = rand(11111, 999990);
        $add = DB::table('speciality')->insert([
                    'speciality_id' => $rand,
                    'speciality_name' => $spl_name,
                    'speciality_value' => $spl_name,
                    'tags' => $decodedtags,
                    'slug' => $slug
            ]);
            if($add){
                   foreach($sub_spl_name as $key=>$value)
                    {
                          $subtags = array();
                          array_push($subtags,$value);
                            $decodedsubtags = json_encode($subtags);
                              $randd = rand(111111, 9999990);
                        DB::table('speciality')->insert([
                                'speciality_id' => $rand,
                                'speciality_name' => $spl_name,
                                'speciality_value' => $spl_name,
                                'speciality_subcategory_id'=> $randd,
                                'speciality_subcategory_id_name'=> $value,
                                'speciality_subcategory_value'=> $value,
                                'tags' => $decodedsubtags
                        ]);
                    }
                
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Speciality Added Successfully.',
                                'slug' => $slug
                    ],200);
         }else{
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
         }
     }
     
       public function AddSubAdmin(Request $request){
            $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'phone' =>'required|unique:users',
        ]);
        if ($validator->fails()) {
            $response = $validator->errors()->first();

            // return response(json_encode($response));
             return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => $response
                ]);
        } else {
            $currtime = date("H:i:s");
           $email =$request->input('email');
           $plat  = $request->input('platform_name');
           $user_type = $request->input('user_type');
           $agency_parent_id =  $request->input('agency_parent_id');
            $month = date('m');
            $year = date("Y");
            $randnum = rand(1111111111,9999999999);
            
            $data = DB::table('users')->insert(
                        [
                        'user_type'=> $user_type,
                        'agency_parent_id' =>$agency_parent_id,
                        'email' => $email,
                        'name' => $request->input('fullname'),
                        'user_id' => $randnum,
                        'phone' => $request->input('phone'),
                        'is_personal_info' => '1',
                        'is_professional_info' => '1',
                        'is_speciality_info' => '1',
                        'is_status' => '1',
                        'is_payment' => '1',
                        'is_completed' => '1',
                        'password' => Hash::make($request->input('password')),
                         'reg_time' => $currtime,
                        ]
            ); 

            if ($data) {
                return response()->json([
                            'ResponseCode' => '1',
                            'ResponseText' => 'Sub Admin Created Successful.'
                ]);
            } else {
                return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => 'Registration UnSuccessful.'
                ]);
            }
        }
         }
         public function DeleteSubAdmin(Request $request,$id){
            //   $user_id = $request->input('user_id');
              $del = DB::table('users')->where([['user_id',$id],['user_type','subadmin']])->delete();
                if ($del) {
                return response()->json([
                            'ResponseCode' => '1',
                            'ResponseText' => 'Sub Admin Deleted Successful.'
                ]);
            } else {
                return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => 'Error.. Please Try Again.'
                ]);
            }
             
         }
          public function UpdateSubAdmin(Request $request){
       
            
            $user_id = $request->input('user_id');
             $status = $request->input('status');
             $user_role = $request->input('user_type');
            $data = DB::table('users')->where('user_id',$user_id)->update(
                        [
                        'name' => $request->input('fullname'),
                        'phone' => $request->input('phone'),
                        'user_status' => $status,
                        'user_type' => $user_role
                        ]
            ); 
            if($request->input('password')){
                   DB::table('users')->where('user_id',$user_id)->update(
                        [
                        'password' => Hash::make($request->input('password')),
                        ]
            ); 
            }

            if ($data) {
                return response()->json([
                            'ResponseCode' => '1',
                            'ResponseText' => 'Sub Admin Updated Successful.'
                ]);
            } else {
                return response()->json([
                            'ResponseCode' => '0',
                            'ResponseText' => 'errorr.'
                ]);
            }
         }
         
         
          public function getUsersManager(Request $request,$id){
              $agencyusers = DB::table('users')->select('*')->where('agency_parent_id',$id)->get();
               return response()->json([
                            'ResponseCode' => '1',
                        'data' => $agencyusers
                ]);
          }

}
