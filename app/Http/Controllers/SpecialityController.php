<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class SpecialityController extends BaseController {
    


   public function getSpeciality(Request $request) {
            
            $user = DB::table('speciality')->get();
            if($user){
                return response()->json([
                     'ResponseCode' => '1',
                        'Data' => $user,
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '0',
                    'ResponseText' => 'error try again',
                ]);
            }
    }
    
      public function getSpecialityCategory(Request $request) {
        // $cateee = DB::table('speciality')->select('speciality_id','speciality_name','spl_logo','slug')->groupby('speciality_id','speciality_name','spl_logo','slug')->get();
          $cateee = DB::table('speciality')->select('speciality_id','speciality_name','spl_logo','slug','spl_desc')->where('speciality_subcategory_id','0')->orderBy('speciality_name')->get();
          
           $spls = json_decode(json_encode($cateee), TRUE);
          if($cateee){
            foreach ($spls as $key => &$value) {
                $splid = $value['speciality_id'];
                $subspls = DB::table('speciality')->select('speciality_subcategory_id','speciality_subcategory_id_name','slug','spl_desc AS description','spl_symtoms AS symptoms','benifits','spl_logo AS logo','symp_image AS symptom_image','desc_image AS description_image')->where('speciality_id',$splid)->where('speciality_subcategory_id','!=','0')->orderBy('speciality_subcategory_id_name','ASC')->get();
                          $subbbbspls = json_decode(json_encode($subspls), TRUE);

                           if($subbbbspls){
                                 $value['sub_spls'] = $subbbbspls;
                            }else{
                                 $value['sub_spls'] = array();
                            }
            }
          }
             if($cateee){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $spls,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $spls,
                    ]);
                }
         }
          public function getSingleSubSpecialityCategory(Request $request,$slug) {
        //   $id =  $request->input('slug');
           
            $cateee = DB::table('speciality')->select('*')->where('slug',$slug)->first();
            $idd = $cateee->speciality_id;
           
            //  $array = explode(',', $id);
        $cateee = DB::table('speciality')->select('speciality_subcategory_id','speciality_subcategory_id_name','slug','spl_desc AS description','spl_symtoms AS symptoms','benifits','spl_logo AS logo','symp_image AS symptom_image','desc_image AS description_image')->where('speciality_id',$idd)->where('speciality_subcategory_id','!=','0')->orderBy('speciality_subcategory_id_name','ASC')->get();
             if($cateee){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }
         }
    
    
       public function getSingleSubSpecialityData(Request $request,$splslug,$slug) {
           
        $cate = DB::table('speciality')->select('*')->where('slug',$splslug)->first();
        if($cate){
               $idd = $cate->speciality_id;
        $cateee = DB::table('speciality')->select('speciality_id','speciality_name','speciality_subcategory_id','speciality_subcategory_id_name','slug','spl_desc AS description','spl_symtoms AS symptoms','benifits','spl_logo AS logo','symp_image AS symptom_image','desc_image AS description_image')->where([['speciality_id',$idd],['slug',$slug]])->first();
             if($cateee){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }
        }else{
              return response()->json([
                          'ResponseCode' => '1',
                                'data' => array(),
                    ]);
        }
     
         }
    
    
    
       public function getSubSpecialityCategory(Request $request) {
           $id =  $request->input('spl_names');
            $array = explode(',', $id);
            $cateee = DB::table('speciality')->select('speciality_name','speciality_subcategory_id','speciality_subcategory_id_name')->whereIn('speciality_name',$array)->where('speciality_subcategory_id','!=','0')->get();
             if($cateee){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }
         }
         
       public function getSubSpecialityGroupedCategory(Request $request) {
          $id =  $request->input('spl_names');
            $array = explode(',', $id);
            $cateee = DB::table('speciality')->select('speciality_id','speciality_name','speciality_subcategory_id','speciality_subcategory_id_name')->whereIn('speciality_name',$array)->where('speciality_subcategory_id','!=','0')->get();
            
            $formatcar = array();
            $cateee = json_decode(json_encode($cateee,true));
            foreach($array as $key => $arr){
                foreach($cateee as $key => $cat){
                    if($arr == $cat->speciality_name){
                        $formatcar[$arr][]= array('id' => $cat->speciality_subcategory_id,'name'=>$cat->speciality_subcategory_id_name,'spl_id'=> $cat->speciality_id,'spl_name'=>$cat->speciality_name);    
                    }
                    // $formatcar[$cat->speciality_name][]= array('id' => $cat->speciality_subcategory_id,'name'=>$cat->speciality_subcategory_id_name,'spl_id'=> $cat->speciality_id,'spl_name'=>$cat->speciality_name);
                }    
            }
            // foreach($cateee as $key => $cat){
            //     $formatcar[$cat->speciality_name][]= array('id' => $cat->speciality_subcategory_id,'name'=>$cat->speciality_subcategory_id_name,'spl_id'=> $cat->speciality_id,'spl_name'=>$cat->speciality_name);
            // }
            
             if(count($formatcar) > 0){
                $allsubs = $formatcar;
            } else{
                 $allsubs = null;
            }
            // print_r(gettype($allsubs)); die();
            
            
            // return $allsubs;
             if($formatcar){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $allsubs,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $formatcar,
                    ]);
                }
         }
        public function getSpecialityID(Request $request) {

                $id =  $request->input('spl_names');
                $array = explode(',', $id);

                // print_r($array);
                $cateee = DB::table('speciality')->select('speciality_id')->whereIn('speciality_name',$array)->groupby('speciality_id')->get();
             if($cateee){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                    ]);
                }
         }
         
         
      public function getSingleSpeciality(Request $request,$id) {
        $cateee = DB::table('speciality')->select('*')->where('slug',$id)->first();
        $idd = $cateee->speciality_id;


            // -------testimonials--------------------
            $maintestimonial = DB::table('testimonials')->select('*')->where([['spl_id',$idd],['spl_category','main']])->get();
            $statetestimonial = DB::table('testimonials')->select('*')->where([['spl_id',$idd],['spl_category','state']])->get();
            $citytestimonial = DB::table('testimonials')->select('*')->where([['spl_id',$idd],['spl_category','city']])->get();
            
            
            

            $cateee = DB::table('speciality')->select('*')->where('speciality_id',$idd)->first();
            $spl_symtoms = $cateee->spl_symtoms;
            if($spl_symtoms){
                 $splsymparr = explode(",",$spl_symtoms);
                 $jsonenc = json_encode($splsymparr);
            }else{
                 $splsymparr = array();
                 $jsonenc = json_encode($splsymparr);
            }
            $benifits =$cateee->benifits;
            if($benifits){
                 $benifitsarr = explode(",",$benifits);
                 $jsonenc = json_encode($benifitsarr);
            }else{
                 $benifitsarr = array();
                 $jsonenc = json_encode($benifitsarr);
            }
            $cateee->spl_sympotoms_array = $jsonenc;
            $cateee->spl_sympotoms_arraydecod = $splsymparr;
            $cateee->benifits_decoded = $benifitsarr;
            
            $state =   DB::table('speciality_state')->select('*')->where('speciality_id',$idd)->first();
            if($state){
               $state_benifits = $state->spl_benifits;
            if($state_benifits){
                 $benifits = explode(",",$state_benifits);
                 $jsonenc = json_encode($benifits);
            }else{
                 $benifits = array();
                 $jsonenc = json_encode($benifits);
            }
            $state->state_spl_benifits = $benifits;
            
            
                if($statetestimonial){
                    $state->testimonials = $statetestimonial;
                }else{
                    $state->testimonials = '';
                }
             
            }
            $city =   DB::table('speciality_city')->select('*')->where('speciality_id',$idd)->first();
            if($city){
                $state_benifits = $city->spl_benifits;
                if($state_benifits){
                     $benifits = explode(",",$state_benifits);
                     $jsonenc = json_encode($benifits);
                }else{
                     $benifits = array();
                     $jsonenc = json_encode($benifits);
                }
                $city->city_spl_benifits = $benifits;
                
                if($citytestimonial){
                $city->testimonials = $citytestimonial;
                    }else{
                        $city->testimonials = '';
                    }
                
            }
            
            if($maintestimonial){
                $cateee->testimonials = $maintestimonial;
            }else{
                $cateee->testimonials = '';
            }
             
          
            
         
             if($cateee){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                                'statedata' => $state,
                                'citydata' => $city,
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'data' => $cateee,
                                'statedata' => $state,
                                'citydata' => $city,
                    ]);
                }
         }
         
         
        public function UpdateDescSpeciality(Request $request) {
                $slug =  $request->input('slug');
                $cateee = DB::table('speciality')->select('*')->where('slug',$slug)->first();
                $id = $cateee->speciality_id;
                
                // $id =  $request->input('speciality_id');
                $spl_desc =  $request->input('spl_desc');
                $spl_symtoms =  $request->input('spl_symtoms');
                $videourl =  $request->input('videourl');
                $video_desc =  $request->input('video_desc');
                $speciality_name =  $request->input('speciality_name');
                if($request->file('graphicurl')){
                     $graphicurl = md5(rand(11111, 999990) . date('Ymdhis')) . "." . $request->file('graphicurl')->getClientOriginalExtension();
                }
                 if($request->file('spl_logo')){
                    $spl_logo = md5(rand(11111, 999990) . date('Ymdhis')) . "." . $request->file('spl_logo')->getClientOriginalExtension();
                 }
                $path = getcwd();
                 $dir = $path.'/speciality';
                // $cateee = DB::table('speciality')->select('*')->where('speciality_id',$id)->first();
                
                $cateee =   DB::table('speciality')->where([['speciality_id',$id],['speciality_subcategory_id','0']])
                    ->update([  
                            'spl_desc' => $spl_desc,
                            'spl_symtoms' => $spl_symtoms,
                            'videourl' => $videourl,
                            'video_desc' => $video_desc,
                            // 'graphicurl' => $graphicurl,
                       ]);
                    if($request->input('speciality_name')){
                        $speciality_name =  $request->input('speciality_name');
                         $slug = preg_replace('#[ -]+#', '-', $speciality_name);
                       $singspl = DB::table('speciality')->select('*')->where([['speciality_id',$id],['speciality_subcategory_id','0']])->first();
                       if($singspl){
                          $tag =  $singspl->tags;
                          if($tag){
                              $decodtag = json_decode($tag);
                          }else{
                              $decodtag = array();
                          }
                       }
                            
                            array_push($decodtag,$speciality_name);
                            $decodedsubtags = json_encode($decodtag);
                        
                        DB::table('speciality')->where('speciality_id',$id)
                            ->update([ 'speciality_name' => $speciality_name,'speciality_value'=>$speciality_name]);
                             DB::table('speciality')->where([['speciality_id',$id],['speciality_subcategory_id','0']])
                            ->update([ 'tags' => $decodedsubtags,'slug'=>$slug]);
                    }
                    if($request->file('graphicurl')){
                        DB::table('speciality')->where([['speciality_id',$id],['speciality_subcategory_id','0']])
                            ->update([ 'graphicurl' => $graphicurl]);
                    }
                     if($request->file('spl_logo')){
                        DB::table('speciality')->where([['speciality_id',$id],['speciality_subcategory_id','0']])
                            ->update([ 'spl_logo' => $spl_logo]);
                     }
        
        if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                     if($request->file('graphicurl')){
                       $request->file('graphicurl')->move("$dir", $graphicurl);
                    }
                     if($request->file('spl_logo')){
                        $request->file('spl_logo')->move("$dir", $spl_logo);
                     }
                   
             if($cateee){
                  
                 
                     return response()->json([
                          'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }else{
                      return response()->json([
                          'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }
         }
           public function addStateSplDescp(Request $request) {
                $speciality_id =  $request->input('speciality_id');
                $title =  $request->input('title');
                $subtitle =  $request->input('subtitle');
                $description =  $request->input('description');
                $spl_benifits =  $request->input('spl_benifits');
                // $state =  $request->input('state');
                $video_url =  $request->input('video_url');
                $rand = rand(11111, 999990);
                $check =   DB::table('speciality_state')->select('*')->where('speciality_id',$speciality_id)->first();
                if($check){
                     $cateee =   DB::table('speciality_state')->where('speciality_id',$speciality_id)
                    ->update([ 
                            'title' => $title,
                            'subtitle'=>$subtitle,
                            'description' => $description,
                            'video_url' => $video_url,
                            'spl_benifits'=>$spl_benifits
                       ]);
                }else{
                     $cateee =   DB::table('speciality_state')
                    ->insert([  
                            'speciality_state_id' => $rand,
                            'speciality_id' => $speciality_id,
                            'title' => $title,
                            'subtitle'=>$subtitle,
                            'description' => $description,
                            // 'state' => $state,
                            'video_url' => $video_url,
                            'spl_benifits'=>$spl_benifits
                       ]);
                }
             if($cateee){
                   return response()->json([
                          'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }else{
                    return response()->json([
                          'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }
         }
          public function addCitySplDescp(Request $request) {
                $speciality_id =  $request->input('speciality_id');
                $title =  $request->input('title');
                $subtitle =  $request->input('subtitle');
                $description =  $request->input('description');
                $spl_benifits =  $request->input('spl_benifits');
                $video_url =  $request->input('video_url');
                $rand = rand(11111, 999990);
                $check =   DB::table('speciality_city')->select('*')->where('speciality_id',$speciality_id)->first();
                if($check){
                     $cateee =   DB::table('speciality_city')->where('speciality_id',$speciality_id)
                    ->update([ 
                            'title' => $title,
                            'subtitle'=>$subtitle,
                            'description' => $description,
                            'video_url' => $video_url,
                            'spl_benifits'=>$spl_benifits
                       ]);
                }else{
                     $cateee =   DB::table('speciality_city')
                    ->insert([  
                            'speciality_city_id' => $rand,
                            'speciality_id' => $speciality_id,
                            'title' => $title,
                            'subtitle'=>$subtitle,
                            'description' => $description,
                            // 'city' => $city,
                            'video_url' => $video_url,
                            'spl_benifits'=>$spl_benifits
                       ]);
                }
             if($cateee){
                   return response()->json([
                          'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }else{
                     return response()->json([
                          'ResponseCode' => '1',
                                'ResponseText' => 'done',
                    ]);
                }
         }
         
         public function addTestimonial(Request $request){
              $spl_category =  $request->input('spl_category');
              $user_name =  $request->input('user_name');
              $description =  $request->input('description');
              $spl_id =  $request->input('spl_id');
              if($request->file('user_image')){
                                $user_image =  md5(rand(11111, 999990) . date('Ymdhis')) . "." . $request->file('user_image')->getClientOriginalExtension();
              }
                // $content = $request->input('content');
               
               $path = getcwd();
                $validator = Validator::make($request->all(), [
                    'user_image' => 'required|file',
                ]);
                if ($validator->fails()) {
                     return response()->json([
                        'ResponseCode' => '0',
                        'ResponseText' => $validator->errors()->first(),
                    ]);
                    
                } else {
                    $dir = $path.'/testimonials';
                }
               $add = DB::table('testimonials')->insert([
                        'spl_id' => $spl_id,
                        'spl_category' => $spl_category,
                        'username' => $user_name,
                        'userimage' => $user_image,
                        'content' => $description
                ]);
                
                
                if($add){
                      if (!is_dir($dir)) {
                            mkdir($dir);
                        }
        
                        $request->file('user_image')->move("$dir", $user_image);
                     return response()->json([
                                        'ResponseCode' => '1',
                                        'ResponseText' => 'Testimonial Added Successfully.',
                            ],200);
                 }else{
                     return response()->json([
                                        'ResponseCode' => '0',
                                        'ResponseText' => 'Error Occured, please try again.',
                            ],400);
                 }
         }
         
}
