<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Carbon\Carbon;
use Validator;


class BlogController extends BaseController {

    public function addBlog(Request $request) {
        $blog_heading = $request->input('blog_heading');
        $meta_title = $request->input('meta_title');
        $meta_desc = $request->input('meta_desc');
        $desc = $request->input('blog_description');
        $blogid = rand(1111111111,9999999999);
        $author = $request->input('blog_author');
        $url = str_replace(" ","-",$blog_heading);
        $date_2 = date("F j, Y");
        if($request->file('blog_image')){
            $blog_image = md5(rand(11111, 999990) . date('Ymdhis')) . "." . $request->file('blog_image')->getClientOriginalExtension();
        }

        $validator = Validator::make($request->all(), [
                    'blog_heading' => 'required|unique:blogs',
                    'blog_image' => 'required|file',
        ]);
        if ($validator->fails()) {
            $response = $validator->errors()->first();

            return response(json_encode($response));
        } else {


        $path = getcwd();
        $dir = $path.'/blogimages';
        $data = DB::table('blogs')->insert([
            'blog_id' => $blogid,
            'blog_heading' => $blog_heading,
            'meta_desc' => $meta_desc,
            'meta_title' => $meta_title,
            'blog_description' => $desc,
            'Author' =>$author,
            'blog_image' =>$blog_image,
            'blog_date' => $date_2, 
            'blog_url' => $url, 
            ]
        ); 
        }
        if ($data) {
            if (!is_dir($dir)) {
                mkdir($dir);
            }
            $request->file('blog_image')->move("$dir", $blog_image);

            return response()->json([
                        'ResponseCode' => '1',
                        'ResponseText' => 'Blog Added Successful.'
            ]);
        } else {
            return response()->json([
                        'ResponseCode' => '0',
                        'ResponseText' => 'Error .. please try again.'
            ]);
        }
    }

    public function UpdateBlog(Request $request) {
        $blog_heading = $request->input('blog_heading');
        $desc = $request->input('blog_description');
        $meta_title = $request->input('meta_title');
        $meta_desc = $request->input('meta_desc');
        $blogid = $request->input('blogid');
        $author = $request->input('blog_author');
        $url = str_replace(" ","-",$blog_heading);
        // $date_2 = date("Y-m-d");
        if($request->file('blog_image')){
            $blog_image = md5(rand(11111, 999990) . date('Ymdhis')) . "." . $request->file('blog_image')->getClientOriginalExtension();
        }
        $path = getcwd();
        $dir = $path.'/blogimages';
        $data = DB::table('blogs')->where('blog_id',$blogid)->update([
            'blog_heading' => $blog_heading,
            'blog_description' => $desc,
            'Author' =>$author,
            'meta_desc' => $meta_desc,
            'meta_title' => $meta_title,
            // 'blog_image' =>$blog_image,
            'blog_url' => $url, 
            ]
        ); 
        if ($data) {
            if($request->file('blog_image')){
                 DB::table('blogs')->where('blog_id',$blogid)->update([
                    'blog_image' =>$blog_image,
                    ]
                ); 
                if (!is_dir($dir)) {
                    mkdir($dir);
                }
                $request->file('blog_image')->move("$dir", $blog_image);
            }
            return response()->json([
                        'ResponseCode' => '1',
                        'ResponseText' => 'Blog Updated Successful.'
            ]);
        } else {
            return response()->json([
                'ResponseCode' => '1',
                'ResponseText' => 'Blog Updated Successful.'
    ]);
        }
    }
    public function getBlogs(Request $request) {
        $allblogs = DB::table('blogs')->select('*')->get();
        if ($allblogs) {
            return response()->json([
                        'ResponseCode' => '1',
                        'data' => $allblogs
            ]);
        } else {
            return response()->json([
                        'ResponseCode' => '1',
                        'data' => $allblogs
            ]);
        }
    }
    public function getSingleBlog(Request $request,$id) {
        $allblogs = DB::table('blogs')->select('*')->where('blog_url',$id)->first();
        if ($allblogs) {
            return response()->json([
                        'ResponseCode' => '1',
                        'data' => $allblogs
            ]);
        } else {
            return response()->json([
                        'ResponseCode' => '1',
                        'data' => $allblogs
            ]);
        }
    }
    public function deleteBlogs(Request $request,$id) {
        $allblogs = DB::table('blogs')->where('blog_id',$id)->delete();
        if($allblogs) {
            return response()->json([
                        'ResponseCode' => '1',
                        'ResponseText' => 'deleted succesfully'
            ]);
        } else {
            return response()->json([
                        'ResponseCode' => '1',
                        'ResponseText' => 'error.. please try again'
            ]);
        }
    }
}