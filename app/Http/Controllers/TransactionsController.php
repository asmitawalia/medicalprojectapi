<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class TransactionsController extends BaseController {
    
      public function getTransactions(Request $request) {
           $user = DB::table('membership_transactions')->select('*')->get();
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }
      }
      public function getSingleTransaction(Request $request ,$id) {
           $user = DB::table('membership_transactions')->select('*')->where('transaction_id',$id)->first();
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }
      }
}