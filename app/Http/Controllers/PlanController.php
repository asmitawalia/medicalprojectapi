<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class PlanController extends BaseController {
    
      public function getPlans(Request $request) {
          $plans =   DB::table('user_plans')->select('*')->where('status','active')->get();
            
         
             return response()->json([
                                'ResponseCode' => '1',
                                'data' => $plans,
                    ],200);
        
      }
       public function getSinglePlans(Request $request,$id) {
          $plan =   DB::table('user_plans')->select('*')->where([['status','active'],['uniquecode',$id]])->first();
            
         
             return response()->json([
                                'ResponseCode' => '1',
                                'data' => $plan,
                    ],200);
        
      }
}