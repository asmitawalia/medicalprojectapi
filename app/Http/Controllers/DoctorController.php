<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class DoctorController extends BaseController {
    
      public function saveleads(Request $request) {
          $agency_id = $request->input('agency_id');
          $name = $request->input('name');
          $email = $request->input('email');
          $phone =  $request->input('phone');
          $message = $request->input('message');
          $leadid = rand(11111, 999990);
          $up =   DB::table('agencyleads')
            ->insert([  
                'lead_id' => $leadid,
                            'name' => $name,
                            'agency_id' => $agency_id,
                            'email' => $email,
                            'phone' => $phone,
                            'message' => $message,
                       ]);
         if($up){
             return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'You will be notify soon.',
                    ],200);
         }else{
             return response()->json([
                                'ResponseCode' => '0',
                                'ResponseText' => 'Error Occured, please try again.',
                    ],400);
         }
      }
      public function getLeads(Request $request ,$id) {
           $user = DB::table('agencyleads')->select('*')->where('agency_id',$id)->get();
                if($user){
                     return response()->json([
                          'ResponseCode' => '1',
                                'data' => $user,
                    ]);
                }else{
                     return response()->json([
                             'ResponseCode' => '0',
                            'ResponseText' => 'error try again',
                        ]);
                }
      }
}