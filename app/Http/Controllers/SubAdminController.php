<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class SubAdminController extends BaseController {
    
public function getSubAdmins(Request $request) {
            
       $data = DB::table('users')->select('*')->where('user_type','subadmin')->get();
            if($data){
                return response()->json([
                     'ResponseCode' => '1',
                        'Data' => $data,
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '1',
                        'Data' => $data,
                ]);
            }
    }
    public function deletesubadmin(Request $request,$id) {
            
             $data = DB::table('users')->where('user_id',$id)->delete();
            if($data){
                return response()->json([
                     'ResponseCode' => '1',
                     'ResponseText' => 'sub admin deleted successfully',
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '1',
                     'ResponseText' => 'error.. please try again',
                ]);
            }
    }
      public function UpdateSubadmin(Request $request) {
              $user_id = $request->input('user_id');
              $up =   DB::table('users')
            ->where('user_id', $user_id)
            ->update([  
                            'name' => $request->input('fullname'),
                            'phone' => $request->input('phone'),
                            'email' => $request->input('email'),
                       
                       ]);
            if($up){
                return response()->json([
                     'ResponseCode' => '1',
                     'ResponseText' => 'details updated successfully',
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '1',
                     'ResponseText' => 'details updated successfully',
                ]);
            }
    }
         public function enabledisableStatus(Request $request) {
              $user_id = $request->input('user_id');
               $enabledisable = $request->input('enabledisable');
               if($enabledisable == 'enable'){
                   $mess = 'status Enabled';
               }else{
                   $mess = 'status Disabled';
               }
              $up =   DB::table('users')
            ->where('user_id', $user_id)
            ->update([  
                            'subadmin_status' => $enabledisable,
                       
                       ]);
            if($up){
                return response()->json([
                     'ResponseCode' => '1',
                     'ResponseText' => $mess,
                ]);
            }else{
                return response()->json([
                     'ResponseCode' => '1',
                     'ResponseText' => $mess,
                ]);
            }
    }
    
}
