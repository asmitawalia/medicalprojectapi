<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Carbon\Carbon;

use Validator;

class LeadsController extends BaseController {
    
  public function saveleads(Request $request) {
    $agency_id = $request->input('agency_id');
    $firstname = $request->input('firstname');
    $lastname = $request->input('lastname');
    $email = $request->input('email');
    $phone =  $request->input('phone');
    $message = $request->input('message');
    $state = $request->input('state');
    $city = $request->input('city');
    $speciality = $request->input('speciality');
    $dd = date("d");
    $mm = date("m");
    $yyyy = date("Y");
    $leadid = rand(11111, 999990);
    $up =   DB::table('agencyleads')
            ->insert([  
            'lead_id' => $leadid,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'agency_id' => $agency_id,
            'email' => $email,
            'phone' => $phone,
            'state' => $state,
            'city' => $city,
            'speciality' => $speciality,
            'message' => $message,
            'dd'=> $dd,
            'mm' => $mm,
            'yyyy' => $yyyy
            ]);
    if($up){
      if($agency_id){
        try {
          $user = DB::table('users')->select('*')->where('user_id',$agency_id)->first();
          $phone = $user->hospital_phone;
          $account_sid = getenv("TWILIO_SID");
          $auth_token = getenv("TWILIO_AUTH_TOKEN");
          $twilio_number = getenv("TWILIO_NUMBER");
          $link = 'https://www.usahealthmd.com/login';
          // $phone = $request->input('phone');
          $message = "somebody contacted you through USA Health MD.click here to View ".$link;
          $correctmessage = str_replace("%20"," ",$message);
          $client = new Client($account_sid, $auth_token);
          $gg = $client->messages->create($phone, 
          ['from' => $twilio_number, 'body' => $correctmessage] );
        }catch (\Exception $e){
					if($e->getCode() == 21211)
					{

					}
	      } 
      }   
      return response()->json([
        'ResponseCode' => '1',
        'ResponseText' => 'You will be notify soon.',
      ],200);
    }else{
      return response()->json([
        'ResponseCode' => '0',
        'ResponseText' => 'Error Occured, please try again.',
      ],400);
     }
  }
   public function upDateStatus(Request $request){
       $lead_id =  $request->input('lead_id');
       $status = $request->input('status');
        $up =   DB::table('agencyleads')->where('lead_id',$lead_id)
                ->update([  
                    'lead_status' => $status,
                           ]);
         return response()->json([
                        'ResponseCode' => '1',
                        'ResponseText' => 'Status Updated succesfully.',
            ],200);
   }
  public function getLeads(Request $request ,$id,$pagenum,$status,$date,$name) {
      if($pagenum == '1'){
            $skip = '0';
        }else{
             $skip = ($pagenum - 1)*10;
        }
        if($status != '0'){
             $user = DB::table('agencyleads')->select('*')->where([['agency_id',$id],['lead_status',$status]])->orderBy('id','desc')->skip($skip)->take('10')->get();
        }else if($date != '0'){
             $user = DB::table('agencyleads')->select('*')->where([['agency_id',$id]])->orderBy('datetime','desc')->skip($skip)->take('10')->get();
        }else if($name != '0'){
             $user = DB::table('agencyleads')->select('*')->where([['agency_id',$id]])->orderBy('firstname','desc')->skip($skip)->take('10')->get();
        }else{
            // echo $skip;
             $user = DB::table('agencyleads')->select('*')->where('agency_id',$id)->skip($skip)->take('10')->orderBy('id','desc')->get();
        }
        $count = DB::table('agencyleads')->select('*')->where('agency_id',$id)->count();
        
         $pendingcount = DB::table('agencyleads')->select('*')->where([['agency_id',$id],['lead_status','pending']])->count();
          $wincount = DB::table('agencyleads')->select('*')->where([['agency_id',$id],['lead_status','won']])->count();
           $lostcount = DB::table('agencyleads')->select('*')->where([['agency_id',$id],['lead_status','lost']])->count();
           
        $array2 = array('data'=> $user,'count'=>$count,'count_win'=>$wincount,'count_lost'=>$lostcount,'count_pending'=>$pendingcount);
    if($user){
         
         return response()->json([
          'ResponseCode' => '1',
          'data' => $array2,
        ]);
    }else{
         return response()->json([
          'ResponseCode' => '1',
          'data' => $array2,
        ]);
    }
  }

  public function addComment(Request $request) {
    date_default_timezone_set("America/Chicago");

    $agency_id = $request->input('agency_id');
    $agencydetails = DB::table('users')->select('hospital_name')->where('user_id',$agency_id)->first();
    //   print_r($agencydetails); die();
    if($agencydetails){
       $cmntname = $agencydetails->hospital_name;
    }else{
        $cmntname = $agency_id;
    }
    $comment = $request->input('comment');
    $lead_id = $request->input('lead_id');
    $comentid = rand(11111, 999990);
    $reg_date = date("Y-m-d");
    $reg_time = date("h:i A");
    $user = DB::table('agencyleads')->select('comments')->where('lead_id',$lead_id)->first();
    $comments = $user->comments;
    if($comments){
          $array1 = json_decode($comments);
    }else{
          $array1 = array();
    }
    $array2=array('id'=>'64564'.$comentid,'comment'=>$comment,'commentby'=>$cmntname,'date'=>$reg_date,'time'=>$reg_time);
    array_push($array1 , $array2);
    $commentsss = json_encode($array1);
    $up =   DB::table('agencyleads')->where('lead_id',$lead_id)
    ->update([  
        'comments' => $commentsss,
               ]);
    if($up){
     return response()->json([
                        'ResponseCode' => '1',
                        'ResponseText' => 'Comment added succesfully.',
            ],200);
    }else{
     return response()->json([
                        'ResponseCode' => '0',
                        'ResponseText' => 'Error Occured, please try again.',
            ],400);
    }
  }
  
  public function getSingleLead(Request $request ,$id) {
    $user = DB::table('agencyleads')->select('*')->where('lead_id',$id)->first();
    if($user){
         return response()->json([
              'ResponseCode' => '1',
                    'data' => $user,
        ]);
    }else{
      return response()->json([
        'ResponseCode' => '0',
        'ResponseText' => 'error try again',
      ]);
    }
  }
  public function get7daysleadsCount(Request $request,$id){   
        $currdate = date('j');
  
        $m= date("m"); // Month value
        $de= date("d"); //today's date
        $y= date("Y"); // Year value
        $daysarray = array();
         for($i=0; $i<=6; $i++){
              
            $nodatedb = date('d-m-y',mktime(0,0,0,$m,($de-$i),$y)); 
            $dt = date('j',mktime(0,0,0,$m,($de-$i),$y)) ;
            $varmonth = date("M", mktime(0, 0, 0, date('n',mktime(0,0,0,$m,($de-$i),$y)), 1));
            $full_date = $dt.' '.$varmonth;
          
            
            $call_action = DB::table('user_actions')->select('*')->where([['mm',date('n',mktime(0,0,0,$m,($de-$i),$y))],['dd',date('j',mktime(0,0,0,$m,($de-$i),$y))],['yyyy',date('Y',mktime(0,0,0,$m,($de-$i),$y))],['agency_id',$id],['action_type','call_action']])->get();
            $countcallaction = count($call_action);
            
            
            $visitor_action = DB::table('user_actions')->select('*')->where([['mm',date('n',mktime(0,0,0,$m,($de-$i),$y))],['dd',date('j',mktime(0,0,0,$m,($de-$i),$y))],['yyyy',date('Y',mktime(0,0,0,$m,($de-$i),$y))],['agency_id',$id],['action_type','visitor_action']])->get();
            $countvisitoraction = count($visitor_action);


            $agency_action = DB::table('agencyleads')->select('*')->where([['mm',date('n',mktime(0,0,0,$m,($de-$i),$y))],['dd',date('j',mktime(0,0,0,$m,($de-$i),$y))],['yyyy',date('Y',mktime(0,0,0,$m,($de-$i),$y))],['agency_id',$id]])->get();
            $countagencyaction = count($agency_action);
             
            $daysarray[$full_date]['call_action'] = $countcallaction;
            $daysarray[$full_date]['form_action'] = $countagencyaction;
            $daysarray[$full_date]['visitor_action'] = $countvisitoraction;
         }
         
         
         $montharray = array();
         for ($ii=0; $ii<=$m-1; $ii++) { 
            $mm = date('n', strtotime("-$ii month"));
            $mnthname = date('M', strtotime("-$ii month"));
            $yy =date('Y', strtotime("-$ii month"));
            
            $call_action = DB::table('user_actions')->select('*')->where([['mm',$mm],['yyyy',$yy],['agency_id',$id],['action_type','call_action']])->get();
            $monthcountcallaction = count($call_action);
            
            
            $visitor_action = DB::table('user_actions')->select('*')->where([['mm',$mm],['yyyy',$yy],['agency_id',$id],['action_type','visitor_action']])->get();
            $monthcountvisitoraction = count($visitor_action);


            $agency_action = DB::table('agencyleads')->select('*')->where([['mm',$mm],['yyyy',$yy],['agency_id',$id]])->get();
            $monthcountagencyaction = count($agency_action);
            
            
            $montharray[$mnthname]['call_action'] = $monthcountcallaction;
            $montharray[$mnthname]['form_action'] = $monthcountagencyaction;
            $montharray[$mnthname]['visitor_action'] = $monthcountvisitoraction;
            
         } 
          return response()->json([
                      'ResponseCode' => '1',
                      'data' => array(
                                // 'leadsData' => $bydays,
                                '7days' => $daysarray,
                                'months' => $montharray
                                )
                    ]);

  } 
  
  
   public function newsletteremail(Request $request){ 
        $email =  $request->input('email');
        $checkemail =  DB::table('newsletter_emails')->select('*')->where('email',$email)->first();
        if($checkemail){
             return response()->json([
                    'ResponseCode' => '1',
                    'ResponseText' => 'Glad to see you again, You are already subscribed',
                  ]);
        }
        $up =   DB::table('newsletter_emails')
                ->insert([  
                    'email' => $email,
                           ]);
       if($up){
             return response()->json([
                'ResponseCode' => '1',
                'ResponseText' => 'Thank you for subscribing',
              ]);
        }else{
          return response()->json([
            'ResponseCode' => '0',
            'ResponseText' => 'OOPS! Something went wrong',
          ]);
        }
   }
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
}