<?php
namespace App\Http\Middleware;
use Closure;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
class UserMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('X-Auth-Token');
      
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'ResponseCode' => '-1',
                'ResponseText' => 'Token not provided.'
            ], 401);
        }
       
        
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
            //   print_r($credentials);
        } catch(ExpiredException $e) {
            return response()->json([
                'ResponseCode' => '-1',
                'ResponseText' => 'Provided token is expired.'
            ], 400);
        } catch(Exception $e) {
            // print_r($e);
            return response()->json([
                'ResponseCode' => '-1',
                'ResponseText' => 'An error while decoding token.'
            ], 400);
        }
        $user = User::where('user_id', $credentials->sub)->first();
     
        // Now let's put the user in the request class so that you can grab it from there
        $request->auth = $user;
        return $next($request);
    }
}